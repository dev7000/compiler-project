template class Number_Ast<double>;
template class Number_Ast<int>;

Eval_Result & Ast::get_value_of_evaluation(Local_Environment & eval_env){

}

void Ast::set_value_of_evaluation(Local_Environment & eval_env, Eval_Result & result){

}

Eval_Result & Ast::evaluate(Local_Environment & eval_env, ostream & file_buffer){

}

/*********************************************************************************************************************************/

template <class T>
Eval_Result & Number_Ast<T>::evaluate(Local_Environment & eval_env, ostream & file_buffer){
	Eval_Result * res;
	if(is_same<T, int>::value)
		res = new Eval_Result_Value_Int();
	if(is_same<T, double>::value)
		res = new Eval_Result_Value_Double();
	res->set_value(constant);
	return *res;
}

/*********************************************************************************************************************************/

void Name_Ast::print_value(Local_Environment & eval_env, ostream & file_buffer){
	file_buffer<<"\n";
	if((*eval_env.get_variable_value(variable_symbol_entry->get_variable_name())).is_variable_defined()==0){
		file_buffer<<VAR_SPACE<<variable_symbol_entry->get_variable_name()<<" : undefined\n";
	}
	else if(variable_symbol_entry->get_data_type()==int_data_type){
		file_buffer<<VAR_SPACE<<variable_symbol_entry->get_variable_name()<<" : "<<(*eval_env.get_variable_value(variable_symbol_entry->get_variable_name())).get_int_value()<<"\n";
	}
	else if(variable_symbol_entry->get_data_type()==double_data_type){
		file_buffer<<VAR_SPACE<<variable_symbol_entry->get_variable_name()<<" : "<<(*eval_env.get_variable_value(variable_symbol_entry->get_variable_name())).get_double_value()<<"\n";
	}
	file_buffer<<"\n";
}

Eval_Result & Name_Ast::get_value_of_evaluation(Local_Environment & eval_env){
	if(eval_env.does_variable_exist(variable_symbol_entry->get_variable_name())){
		return *eval_env.get_variable_value(variable_symbol_entry->get_variable_name());
	}else{
		printf("cs316: Error\n");
	}

}

void Name_Ast::set_value_of_evaluation(Local_Environment & eval_env, Eval_Result & result){
	if(eval_env.does_variable_exist(variable_symbol_entry->get_variable_name())){
		if(node_data_type==int_data_type)
		eval_env.get_variable_value(variable_symbol_entry->get_variable_name())->set_value(result.get_int_value());
		if(node_data_type==double_data_type)
		eval_env.get_variable_value(variable_symbol_entry->get_variable_name())->set_value(result.get_double_value());
	}else{
		printf("cs316: Error\n");
	}
}

Eval_Result & Name_Ast::evaluate(Local_Environment & eval_env, ostream & file_buffer){
	if(eval_env.does_variable_exist(variable_symbol_entry->get_variable_name())){
		if(eval_env.is_variable_defined(variable_symbol_entry->get_variable_name())){
			return *eval_env.get_variable_value(variable_symbol_entry->get_variable_name());
		}else{
			printf("cs316: Error\n");
		}
	}else{
		printf("cs316: Error\n");
	}
}

/*********************************************************************************************************************************/

Eval_Result & Assignment_Ast::evaluate(Local_Environment & eval_env, ostream & file_buffer){
	Eval_Result& rrhs = rhs->evaluate(eval_env,file_buffer);
	lhs->set_value_of_evaluation(eval_env,rrhs);
	print(file_buffer);
	lhs->print_value(eval_env,file_buffer);
	return rrhs;
}

/*********************************************************************************************************************************/

Eval_Result & Arithmetic_Expr_Ast::evaluate(Local_Environment & eval_env, ostream & file_buffer){


}

/*********************************************************************************************************************************/

Eval_Result & UMinus_Ast::evaluate(Local_Environment & eval_env, ostream & file_buffer){
	Eval_Result& rlhs = lhs->evaluate(eval_env,file_buffer);
	if(node_data_type == int_data_type){
		rlhs.set_value(-1*rlhs.get_int_value());
		return rlhs;
	}
	if(node_data_type == double_data_type){
		rlhs.set_value(-1.0*rlhs.get_double_value());
		return rlhs;
	}
}

/*********************************************************************************************************************************/

Eval_Result & Plus_Ast::evaluate(Local_Environment & eval_env, ostream & file_buffer){
	Eval_Result& rlhs = lhs->evaluate(eval_env,file_buffer);
	Eval_Result& rrhs = rhs->evaluate(eval_env,file_buffer);
	if(node_data_type == int_data_type){
		rlhs.set_value(rlhs.get_int_value()+rrhs.get_int_value());
		return rlhs;
	}
	if(node_data_type == double_data_type){
		rlhs.set_value(rlhs.get_double_value()+rrhs.get_double_value());
		return rlhs;
	}
}

/*********************************************************************************************************************************/

Eval_Result & Minus_Ast::evaluate(Local_Environment & eval_env, ostream & file_buffer){
	Eval_Result& rlhs = lhs->evaluate(eval_env,file_buffer);
	Eval_Result& rrhs = rhs->evaluate(eval_env,file_buffer);
	if(node_data_type == int_data_type){
		rlhs.set_value(rlhs.get_int_value()-rrhs.get_int_value());
		return rlhs;
	}
	if(node_data_type == double_data_type){
		rlhs.set_value(rlhs.get_double_value()-rrhs.get_double_value());
		return rlhs;
	}
}

/*********************************************************************************************************************************/

Eval_Result & Divide_Ast::evaluate(Local_Environment & eval_env, ostream & file_buffer){
	Eval_Result& rlhs = lhs->evaluate(eval_env,file_buffer);
	Eval_Result& rrhs = rhs->evaluate(eval_env,file_buffer);
	if(node_data_type == int_data_type){
		rlhs.set_value(rlhs.get_int_value()/rrhs.get_int_value());
		return rlhs;
	}
	if(node_data_type == double_data_type){
		rlhs.set_value(rlhs.get_double_value()/rrhs.get_double_value());
		return rlhs;
	}
}

/*********************************************************************************************************************************/

Eval_Result & Mult_Ast::evaluate(Local_Environment & eval_env, ostream & file_buffer){
	Eval_Result& rlhs = lhs->evaluate(eval_env,file_buffer);
	Eval_Result& rrhs = rhs->evaluate(eval_env,file_buffer);
	if(node_data_type == int_data_type){
		rlhs.set_value(rlhs.get_int_value()*rrhs.get_int_value());
		return rlhs;
	}
	if(node_data_type == double_data_type){
		rlhs.set_value(rlhs.get_double_value()*rrhs.get_double_value());
		return rlhs;
	}
}

/*********************************************************************************************************************************/

Eval_Result & Return_Ast::evaluate(Local_Environment & eval_env, ostream & file_buffer){

}

/*********************************************************************************************************************************/

Eval_Result & Conditional_Expression_Ast::evaluate(Local_Environment & eval_env, ostream & file_buffer){
	Eval_Result& rcond = cond->evaluate(eval_env,file_buffer);
	if(rcond.get_int_value()==1){
		return lhs->evaluate(eval_env,file_buffer);
	}else if(rcond.get_int_value()==0){
		return rhs->evaluate(eval_env,file_buffer);
	}
}

/*********************************************************************************************************************************/

Eval_Result & Relational_Expr_Ast::evaluate(Local_Environment & eval_env, ostream & file_buffer){
	Eval_Result& rlhs = lhs_condition->evaluate(eval_env,file_buffer);
	Eval_Result& rrhs = rhs_condition->evaluate(eval_env,file_buffer);
	if(rel_op == less_equalto){
		if(node_data_type == int_data_type){
			if(rlhs.get_int_value()<=rrhs.get_int_value()){
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(1);
				return *res;
			}else{
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(0);
				return *res;
			}
		}
		if(node_data_type == double_data_type){
			if(rlhs.get_double_value()<=rrhs.get_double_value()){
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(1);
				return *res;
			}else{
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(0);
				return *res;
			}
		}
	}
	if(rel_op == less_than){
		if(node_data_type == int_data_type){
			if(rlhs.get_int_value()<rrhs.get_int_value()){
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(1);
				return *res;
			}else{
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(0);
				return *res;
			}
		}
		if(node_data_type == double_data_type){
			if(rlhs.get_double_value()<rrhs.get_double_value()){
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(1);
				return *res;
			}else{
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(0);
				return *res;
			}
		}
	}
	if(rel_op == greater_than){
		if(node_data_type == int_data_type){
			if(rlhs.get_int_value()>rrhs.get_int_value()){
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(1);
				return *res;
			}else{
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(0);
				return *res;
			}
		}
		if(node_data_type == double_data_type){
			if(rlhs.get_double_value()>rrhs.get_double_value()){
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(1);
				return *res;
			}else{
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(0);
				return *res;
			}
		}
	}
	if(rel_op == greater_equalto){
		if(node_data_type == int_data_type){
			if(rlhs.get_int_value()>=rrhs.get_int_value()){
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(1);
				return *res;
			}else{
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(0);
				return *res;
			}
		}
		if(node_data_type == double_data_type){
			if(rlhs.get_double_value()>=rrhs.get_double_value()){
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(1);
				return *res;
			}else{
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(0);
				return *res;
			}
		}
	}
	if(rel_op == equalto){
		if(node_data_type == int_data_type){
			if(rlhs.get_int_value()==rrhs.get_int_value()){
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(1);
				return *res;
			}else{
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(0);
				return *res;
			}
		}
		if(node_data_type == double_data_type){
			if(rlhs.get_double_value()==rrhs.get_double_value()){
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(1);
				return *res;
			}else{
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(0);
				return *res;
			}
		}
	}
	if(rel_op == not_equalto){
		if(node_data_type == int_data_type){
			if(rlhs.get_int_value()!=rrhs.get_int_value()){
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(1);
				return *res;
			}else{
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(0);
				return *res;
			}
		}
		if(node_data_type == double_data_type){
			if(rlhs.get_double_value()!=rrhs.get_double_value()){
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(1);
				return *res;
			}else{
				Eval_Result * res = new Eval_Result_Value_Int();
				res->set_value(0);
				return *res;
			}
		}
	}


}

/*********************************************************************************************************************************/

Eval_Result & Logical_Expr_Ast::evaluate(Local_Environment & eval_env, ostream & file_buffer){
	if(bool_op == _logical_not){
		Eval_Result& rlhs= lhs_op->evaluate(eval_env,file_buffer);
		Eval_Result * res = new Eval_Result_Value_Int();
		if(rlhs.get_int_value()==0)
			res->set_value(1);
		else
			res->set_value(0);
		return *res;
	}

	if(bool_op == _logical_or){
		Eval_Result& rlhs= lhs_op->evaluate(eval_env,file_buffer);
		Eval_Result& rrhs= rhs_op->evaluate(eval_env,file_buffer);
		Eval_Result * res = new Eval_Result_Value_Int();
		if(rlhs.get_int_value() || rrhs.get_int_value())
			res->set_value(1);
		else
			res->set_value(0);
		return *res;
	}

	if(bool_op == _logical_and){
		Eval_Result& rlhs= lhs_op->evaluate(eval_env,file_buffer);
		Eval_Result& rrhs= rhs_op->evaluate(eval_env,file_buffer);
		Eval_Result * res = new Eval_Result_Value_Int();
		if(rlhs.get_int_value() && rrhs.get_int_value())
			res->set_value(1);
		else
			res->set_value(0);
		return *res;
	}



}

/*********************************************************************************************************************************/

Eval_Result & Selection_Statement_Ast::evaluate(Local_Environment & eval_env, ostream & file_buffer){
	Eval_Result& rcond = cond->evaluate(eval_env,file_buffer);
	Eval_Result * res = new Eval_Result_Value_Int();
	res->set_value(0);

	if(rcond.get_int_value()==1){
		then_part->evaluate(eval_env,file_buffer);
	}else if(rcond.get_int_value()==0){
		if(else_part!=NULL)
			else_part->evaluate(eval_env,file_buffer);
	}

	return *res;
}

/*********************************************************************************************************************************/

Eval_Result & Iteration_Statement_Ast::evaluate(Local_Environment & eval_env, ostream & file_buffer){
	if(is_do_form){
		do{
			body->evaluate(eval_env,file_buffer);
		}while(cond->evaluate(eval_env,file_buffer).get_int_value());
	}else{
		while(cond->evaluate(eval_env,file_buffer).get_int_value()){
			body->evaluate(eval_env,file_buffer);
		}
	}

	Eval_Result * res = new Eval_Result_Value_Int();
	res->set_value(0);

	return *res;
}

/*********************************************************************************************************************************/

Eval_Result & Sequence_Ast::evaluate(Local_Environment & eval_env, ostream & file_buffer){
	for (list<Ast* >::iterator it=statement_list.begin(); it != statement_list.end(); ++it){
		(*it)->evaluate(eval_env,file_buffer);
	}

	Eval_Result * res = new Eval_Result_Value_Int();
	res->set_value(0);

	return *res;
}