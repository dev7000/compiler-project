template class Number_Ast<double>;
template class Number_Ast<int>;


Code_For_Ast & Ast::create_store_stmt(Register_Descriptor * store_register){

}

/*********************************************************************************************************************************/

Code_For_Ast & Assignment_Ast::compile(){
	Code_For_Ast & rres = rhs->compile();
	Code_For_Ast & lres = lhs->create_store_stmt(rres.get_reg());
	list<Icode_Stmt *>::iterator it = lres.get_icode_list().begin();
	rres.append_ics(*(*it));
	rres.get_reg()->reset_register_occupied();
	return rres;
}

Code_For_Ast & Assignment_Ast::compile_and_optimize_ast(Lra_Outcome & lra){

}

/*********************************************************************************************************************************/

Code_For_Ast & Name_Ast::compile(){
	Move_IC_Stmt* stmt;
	if(node_data_type==int_data_type)
		stmt = new Move_IC_Stmt(load,new Mem_Addr_Opd(*variable_symbol_entry),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
	else if(node_data_type==double_data_type)
		stmt = new Move_IC_Stmt(load_d,new Mem_Addr_Opd(*variable_symbol_entry),new Register_Addr_Opd(machine_desc_object.get_new_register<float_reg>()));

	Code_For_Ast* code = new Code_For_Ast();
	code->append_ics(*stmt);
	code->set_reg(stmt->get_result()->get_reg());
	return *code;
}

Code_For_Ast & Name_Ast::create_store_stmt(Register_Descriptor * store_register){
	Move_IC_Stmt* stmt;
	if(node_data_type==int_data_type)
		stmt = new Move_IC_Stmt(store,new Register_Addr_Opd(store_register),new Mem_Addr_Opd(*variable_symbol_entry));
	else if(node_data_type==double_data_type)
		stmt = new Move_IC_Stmt(store_d,new Register_Addr_Opd(store_register),new Mem_Addr_Opd(*variable_symbol_entry));

	Code_For_Ast* code = new Code_For_Ast();
	code->append_ics(*stmt);
	code->set_reg(stmt->get_result()->get_reg());
	return *code;
}

Code_For_Ast & Name_Ast::compile_and_optimize_ast(Lra_Outcome & lra){

}

/*********************************************************************************************************************************/
template <class T>
Code_For_Ast & Number_Ast<T>::compile(){
	Move_IC_Stmt* stmt;
	if(node_data_type==int_data_type)
		stmt = new Move_IC_Stmt(imm_load,new Const_Opd<int>(constant),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
	else if(node_data_type==double_data_type)
		stmt = new Move_IC_Stmt(imm_load_d,new Const_Opd<double>(constant),new Register_Addr_Opd(machine_desc_object.get_new_register<float_reg>()));

	Code_For_Ast* code = new Code_For_Ast();
	code->append_ics(*stmt);
	code->set_reg(stmt->get_result()->get_reg());
	return *code;
}

template <class T>
Code_For_Ast & Number_Ast<T>::compile_and_optimize_ast(Lra_Outcome & lra){

}

/*********************************************************************************************************************************/

Code_For_Ast & Plus_Ast::compile(){
	Code_For_Ast & lres = lhs->compile();
	Code_For_Ast & rres = rhs->compile();
	Compute_IC_Stmt* stmt;
	if(node_data_type==int_data_type)
		stmt = new Compute_IC_Stmt(add,new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
	else if(node_data_type==double_data_type)
		stmt = new Compute_IC_Stmt(add_d,new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<float_reg>()));

	rres.get_reg()->reset_register_occupied();
	lres.get_reg()->reset_register_occupied();

	lres.set_reg(stmt->get_result()->get_reg());

	
	for(list<Icode_Stmt *>::iterator it = rres.get_icode_list().begin();it!=rres.get_icode_list().end();it++){
		lres.append_ics(*(*it));
	}

	lres.append_ics(*stmt);

	return lres;

}

Code_For_Ast & Plus_Ast::compile_and_optimize_ast(Lra_Outcome & lra){

}

/*********************************************************************************************************************************/

Code_For_Ast & Minus_Ast::compile(){
	Code_For_Ast & lres = lhs->compile();
	Code_For_Ast & rres = rhs->compile();
	Compute_IC_Stmt* stmt;
	if(node_data_type==int_data_type)
		stmt = new Compute_IC_Stmt(sub,new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
	else if(node_data_type==double_data_type)
		stmt = new Compute_IC_Stmt(sub_d,new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<float_reg>()));

	rres.get_reg()->reset_register_occupied();
	lres.get_reg()->reset_register_occupied();

	lres.set_reg(stmt->get_result()->get_reg());

	
	for(list<Icode_Stmt *>::iterator it = rres.get_icode_list().begin();it!=rres.get_icode_list().end();it++){
		lres.append_ics(*(*it));
	}

	lres.append_ics(*stmt);
	return lres;
}

Code_For_Ast & Minus_Ast::compile_and_optimize_ast(Lra_Outcome & lra){

}

/*********************************************************************************************************************************/

Code_For_Ast & Divide_Ast::compile(){
	Code_For_Ast & lres = lhs->compile();
	Code_For_Ast & rres = rhs->compile();
	Compute_IC_Stmt* stmt;
	if(node_data_type==int_data_type)
		stmt = new Compute_IC_Stmt(divd,new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
	else if(node_data_type==double_data_type)
		stmt = new Compute_IC_Stmt(div_d,new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<float_reg>()));

	
	rres.get_reg()->reset_register_occupied();
	lres.get_reg()->reset_register_occupied();

	lres.set_reg(stmt->get_result()->get_reg());

	
	for(list<Icode_Stmt *>::iterator it = rres.get_icode_list().begin();it!=rres.get_icode_list().end();it++){
		lres.append_ics(*(*it));
	}

	lres.append_ics(*stmt);

	return lres;
}

Code_For_Ast & Divide_Ast::compile_and_optimize_ast(Lra_Outcome & lra){

}

/*********************************************************************************************************************************/

Code_For_Ast & Mult_Ast::compile(){
	Code_For_Ast & lres = lhs->compile();
	Code_For_Ast & rres = rhs->compile();
	Compute_IC_Stmt* stmt;
	if(node_data_type==int_data_type)
		stmt = new Compute_IC_Stmt(mult,new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
	else if(node_data_type==double_data_type)
		stmt = new Compute_IC_Stmt(mult_d,new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<float_reg>()));

	rres.get_reg()->reset_register_occupied();
	lres.get_reg()->reset_register_occupied();

	lres.set_reg(stmt->get_result()->get_reg());

	
	for(list<Icode_Stmt *>::iterator it = rres.get_icode_list().begin();it!=rres.get_icode_list().end();it++){
		lres.append_ics(*(*it));
	}

	lres.append_ics(*stmt);

	return lres;
}

Code_For_Ast & Mult_Ast::compile_and_optimize_ast(Lra_Outcome & lra){

}

/*********************************************************************************************************************************/

Code_For_Ast & UMinus_Ast::compile(){
	Code_For_Ast & lres = lhs->compile();
	Move_IC_Stmt* stmt;
	if(node_data_type==int_data_type)
		stmt = new Move_IC_Stmt(uminus,new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
	else if(node_data_type==double_data_type)
		stmt = new Move_IC_Stmt(uminus_d,new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<float_reg>()));

	
	lres.get_reg()->reset_register_occupied();

	lres.set_reg(stmt->get_result()->get_reg());
	lres.append_ics(*stmt);
	return lres;
}

Code_For_Ast & UMinus_Ast::compile_and_optimize_ast(Lra_Outcome & lra){

}

/*********************************************************************************************************************************/

Code_For_Ast & Conditional_Expression_Ast::compile(){
	Code_For_Ast & cres = cond->compile();
	string label1 = this->get_new_label();
	string label2 = this->get_new_label();
	Control_Flow_IC_Stmt* stmt = new Control_Flow_IC_Stmt(beq, new Register_Addr_Opd(cres.get_reg()),new Register_Addr_Opd(machine_desc_object.spim_register_table[zero]),label1);
	cres.get_reg()->reset_register_occupied();
	Code_For_Ast & lres = lhs->compile();
	Code_For_Ast & rres = rhs->compile();
	Label_IC_Stmt* l1 = new Label_IC_Stmt(j,label2);
	Label_IC_Stmt* l2 = new Label_IC_Stmt(label,label1);
	Label_IC_Stmt* l3 = new Label_IC_Stmt(label,label2);


	for(list<Icode_Stmt *>::iterator it = lres.get_icode_list().begin();it!=lres.get_icode_list().end();it++){
		cres.append_ics(*(*it));
	}

	cres.append_ics(*l1);
	cres.append_ics(*l2);

	for(list<Icode_Stmt *>::iterator it = rres.get_icode_list().begin();it!=rres.get_icode_list().end();it++){
		cres.append_ics(*(*it));
	}

	cres.append_ics(*l3);

	return cres;
}

/*********************************************************************************************************************************/

Code_For_Ast & Return_Ast::compile(){
	if(node_data_type==void_data_type){
		Code_For_Ast* res = new Code_For_Ast();
		return *res;
	}
	Code_For_Ast & res=return_value->compile();
	Move_IC_Stmt* stmt;
	if(node_data_type==int_data_type)
		stmt = new Move_IC_Stmt(mov,new Register_Addr_Opd(res.get_reg()),new Register_Addr_Opd(machine_desc_object.spim_register_table[v1]));
	else if(node_data_type==double_data_type)
		stmt = new Move_IC_Stmt(move_d,new Register_Addr_Opd(res.get_reg()),new Register_Addr_Opd(machine_desc_object.spim_register_table[f0]));
	res.get_reg()->reset_register_occupied();

	res.set_reg(stmt->get_result()->get_reg());
	res.append_ics(*stmt);

	Label_IC_Stmt* jump = new Label_IC_Stmt(j,"epilogue_"+proc_name);
	res.append_ics(*jump);

	return res;
}	

Code_For_Ast & Return_Ast::compile_and_optimize_ast(Lra_Outcome & lra){

}

/*********************************************************************************************************************************/

Code_For_Ast & Relational_Expr_Ast::compile(){
	Code_For_Ast & lres = lhs_condition->compile();
	Code_For_Ast & rres = rhs_condition->compile();
	Compute_IC_Stmt* stmt;

	if(rel_op == less_equalto){
		if(node_data_type == int_data_type){
			stmt = new Compute_IC_Stmt(sle, new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
		}
		if(node_data_type == double_data_type){
			stmt = new Compute_IC_Stmt(sle_d, new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
		}
	}
	if(rel_op == less_than){
		if(node_data_type == int_data_type){
			stmt = new Compute_IC_Stmt(slt, new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
		}
		if(node_data_type == double_data_type){
			stmt = new Compute_IC_Stmt(slt_d, new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
		}
	}
	if(rel_op == greater_than){
		if(node_data_type == int_data_type){
			stmt = new Compute_IC_Stmt(sgt, new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
		}
		if(node_data_type == double_data_type){
			stmt = new Compute_IC_Stmt(sgt_d, new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
		}
	}
	if(rel_op == greater_equalto){
		if(node_data_type == int_data_type){
			stmt = new Compute_IC_Stmt(sge, new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
		}
		if(node_data_type == double_data_type){
			stmt = new Compute_IC_Stmt(sge_d, new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
		}
	}
	if(rel_op == equalto){
		if(node_data_type == int_data_type){
			stmt = new Compute_IC_Stmt(seq, new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
		}
		if(node_data_type == double_data_type){
			stmt = new Compute_IC_Stmt(seq_d, new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
		}
	}
	if(rel_op == not_equalto){
		if(node_data_type == int_data_type){
			stmt = new Compute_IC_Stmt(sne, new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
		}
		if(node_data_type == double_data_type){
			stmt = new Compute_IC_Stmt(sne_d, new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
		}
	}


	rres.get_reg()->reset_register_occupied();
	lres.get_reg()->reset_register_occupied();
	
	Code_For_Ast* code = new Code_For_Ast();

	for(list<Icode_Stmt *>::iterator it = lres.get_icode_list().begin();it!=lres.get_icode_list().end();it++){
		code->append_ics(*(*it));
	}

	for(list<Icode_Stmt *>::iterator it = rres.get_icode_list().begin();it!=rres.get_icode_list().end();it++){
		code->append_ics(*(*it));
	}

	code->set_reg(stmt->get_result()->get_reg());
	code->append_ics(*stmt);

	return *code;
}

/*********************************************************************************************************************************/

Code_For_Ast & Logical_Expr_Ast::compile(){
	Code_For_Ast & lres = lhs_op->compile();
	Code_For_Ast & rres = rhs_op->compile();
	Compute_IC_Stmt* stmt;

	if(bool_op == _logical_not){
		stmt = new Compute_IC_Stmt(not_t, new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
	}

	if(bool_op == _logical_or){
		stmt = new Compute_IC_Stmt(or_t, new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
	}

	if(bool_op == _logical_and){
		stmt = new Compute_IC_Stmt(and_t, new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(rres.get_reg()),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
	}

	Code_For_Ast* code = new Code_For_Ast();

	for(list<Icode_Stmt *>::iterator it = lres.get_icode_list().begin();it!=lres.get_icode_list().end();it++){
		code->append_ics(*(*it));
	}

	for(list<Icode_Stmt *>::iterator it = rres.get_icode_list().begin();it!=rres.get_icode_list().end();it++){
		code->append_ics(*(*it));
	}

	rres.get_reg()->reset_register_occupied();
	lres.get_reg()->reset_register_occupied();

	code->append_ics(*stmt);
	code->set_reg(stmt->get_result()->get_reg());
	return *code;
}

/*********************************************************************************************************************************/

Code_For_Ast & Selection_Statement_Ast::compile(){
	Code_For_Ast & cres = cond->compile();
	string label1 = this->get_new_label();
	string label2 = this->get_new_label();
	Control_Flow_IC_Stmt* stmt = new Control_Flow_IC_Stmt(beq, new Register_Addr_Opd(cres.get_reg()),new Register_Addr_Opd(machine_desc_object.spim_register_table[zero]),label1);
	cres.get_reg()->reset_register_occupied();
	Code_For_Ast & lres = then_part->compile();
	Code_For_Ast & rres = (else_part == NULL) ? *(new Code_For_Ast()) : else_part->compile();
	Label_IC_Stmt* l1 = new Label_IC_Stmt(j,label2);
	Label_IC_Stmt* l2 = new Label_IC_Stmt(label,label1);
	Label_IC_Stmt* l3 = new Label_IC_Stmt(label,label2);

	cres.append_ics(*stmt);

	for(list<Icode_Stmt *>::iterator it = lres.get_icode_list().begin();it!=lres.get_icode_list().end();it++){
		cres.append_ics(*(*it));
	}

	if(else_part!=NULL){
		cres.append_ics(*l1);
		cres.append_ics(*l2);


		for(list<Icode_Stmt *>::iterator it = rres.get_icode_list().begin();it!=rres.get_icode_list().end();it++){
			cres.append_ics(*(*it));
		}
		cres.append_ics(*l3);
	}else{
		cres.append_ics(*l2);
	}


	return cres;
}

/*********************************************************************************************************************************/

Code_For_Ast & Iteration_Statement_Ast::compile(){
	string label1 = this->get_new_label();
	string label2 = this->get_new_label();

	Label_IC_Stmt* l1 = new Label_IC_Stmt(j,label2);
	Label_IC_Stmt* l2 = new Label_IC_Stmt(label,label1);
	Label_IC_Stmt* l3 = new Label_IC_Stmt(label,label2);

	Code_For_Ast * code = new Code_For_Ast();

	if(is_do_form){
		Code_For_Ast & rres = body->compile();
		Code_For_Ast & lres = cond->compile();
		lres.get_reg()->reset_register_occupied();
		Control_Flow_IC_Stmt* stmt = new Control_Flow_IC_Stmt(bne, new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(machine_desc_object.spim_register_table[zero]),label2);
		code->append_ics(*l2);
		for(list<Icode_Stmt *>::iterator it = rres.get_icode_list().begin();it!=rres.get_icode_list().end();it++){
			code->append_ics(*(*it));
		}
		code->append_ics(*l3);
		for(list<Icode_Stmt *>::iterator it = lres.get_icode_list().begin();it!=lres.get_icode_list().end();it++){
			code->append_ics(*(*it));
		}
		code->append_ics(*stmt);
	}else{
		Code_For_Ast & lres = cond->compile();
		lres.get_reg()->reset_register_occupied();
		Control_Flow_IC_Stmt* stmt = new Control_Flow_IC_Stmt(bne, new Register_Addr_Opd(lres.get_reg()),new Register_Addr_Opd(machine_desc_object.spim_register_table[zero]),label2);
		Code_For_Ast & rres = body->compile();
		code->append_ics(*l1);
		code->append_ics(*l2);
		for(list<Icode_Stmt *>::iterator it = rres.get_icode_list().begin();it!=rres.get_icode_list().end();it++){
			code->append_ics(*(*it));
		}
		code->append_ics(*l3);
		for(list<Icode_Stmt *>::iterator it = lres.get_icode_list().begin();it!=lres.get_icode_list().end();it++){
			code->append_ics(*(*it));
		}
		code->append_ics(*stmt);
	}

	return *code;

}

/*********************************************************************************************************************************/

Code_For_Ast & Sequence_Ast::compile(){
	Code_For_Ast* code = new Code_For_Ast();
	for (list<Ast* >::iterator it1=statement_list.begin(); it1 != statement_list.end(); ++it1){
		Code_For_Ast & temp = (*it1)->compile();
		for(list<Icode_Stmt *>::iterator it = temp.get_icode_list().begin();it!=temp.get_icode_list().end();it++){
			code->append_ics(*(*it));
		}
	}
	return *code;
}

/*********************************************************************************************************************************/

Code_For_Ast & Print_Ast::compile(){
	Move_IC_Stmt* stmt1,*stmt2;
	if(var->get_data_type() == int_data_type){
		stmt1 = new Move_IC_Stmt(imm_load,new Const_Opd<int>(1),new Register_Addr_Opd(machine_desc_object.spim_register_table[v0]));
		stmt2 = new Move_IC_Stmt(load,new Mem_Addr_Opd((((Name_Ast*)var)->get_symbol_entry())),new Register_Addr_Opd(machine_desc_object.spim_register_table[a0]));
	}else if(var->get_data_type() == double_data_type){
		stmt1 = new Move_IC_Stmt(imm_load,new Const_Opd<int>(3),new Register_Addr_Opd(machine_desc_object.spim_register_table[v0]));
		stmt2 = new Move_IC_Stmt(load_d,new Mem_Addr_Opd((((Name_Ast*)var)->get_symbol_entry())),new Register_Addr_Opd(machine_desc_object.spim_register_table[f12]));
	}else{
		printf("asdas\n");
		exit(0);
	}
	Print_IC_Stmt* stmt3 = new Print_IC_Stmt();
	Code_For_Ast* code = new Code_For_Ast();
	code->append_ics(*stmt1);
	code->append_ics(*stmt2);
	code->append_ics(*stmt3);
	code->set_reg(stmt2->get_result()->get_reg());
	return *code;
}
