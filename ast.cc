template class Number_Ast<double>;
template class Number_Ast<int>;

/*********************************************************************************************************************************/

int Ast::labelCounter;

Ast::Ast(){

}

Ast::~Ast(){

}

bool Ast::check_ast(){

}

Symbol_Table_Entry & Ast::get_symbol_entry(){

}

void Ast::set_data_type(Data_Type dt){
	node_data_type = dt;
}


bool Ast::is_value_zero(){

}


Data_Type Ast::get_data_type(){
	return node_data_type;
}

void Ast::print(ostream & file_buffer){
	
}

void Ast::print_value(Local_Environment & eval_env, ostream & file_buffer){
	
}


/*********************************************************************************************************************************/

template <class T>
Number_Ast<T>::Number_Ast(T number, Data_Type constant_data_type, int line){
	node_data_type = constant_data_type;
	lineno = line;
	constant = number;
}

template <class T>
Data_Type Number_Ast<T>::get_data_type(){
	return node_data_type;
}

template <class T>
void Number_Ast<T>::set_data_type(Data_Type dt){
	node_data_type = dt;
}

template <class T>	
bool Number_Ast<T>::is_value_zero(){
	return constant==0;
}

template <class T>
void Number_Ast<T>::print(ostream & file_buffer){
	file_buffer<<"Num : "<<constant;
}


/*********************************************************************************************************************************/

Name_Ast::Name_Ast(string & name, Symbol_Table_Entry & var_entry, int line){
	variable_symbol_entry = &var_entry;
	lineno = line;
	node_data_type = var_entry.get_data_type();
}

Data_Type Name_Ast::get_data_type(){
	return node_data_type;
}

Symbol_Table_Entry & Name_Ast::get_symbol_entry(){
	return *variable_symbol_entry;
}

void Name_Ast::set_data_type(Data_Type dt){
	node_data_type = dt;
}

void Name_Ast::print(ostream & file_buffer){
	file_buffer<<"Name : "<<variable_symbol_entry->get_variable_name();
}


/*********************************************************************************************************************************/

Assignment_Ast::Assignment_Ast(Ast * temp_lhs, Ast * temp_rhs, int line){
	lhs = temp_lhs;
	rhs = temp_rhs;
	lineno = line;
}

bool Assignment_Ast::check_ast(){
	return lhs->get_data_type() == rhs->get_data_type();
}	

void Assignment_Ast::print(ostream & file_buffer){
	file_buffer<<"\n"<<AST_SPACE<<"Asgn:\n";
	file_buffer<<AST_NODE_SPACE<<"LHS (";
	lhs->print(file_buffer);
	file_buffer<<")\n"<<AST_NODE_SPACE<<"RHS (";
	rhs->print(file_buffer);
	file_buffer<<")";
}


/*********************************************************************************************************************************/

Data_Type Arithmetic_Expr_Ast::get_data_type(){
	return node_data_type;
}

void Arithmetic_Expr_Ast::set_data_type(Data_Type dt){
	node_data_type = dt;
}

bool Arithmetic_Expr_Ast::check_ast(){
	return lhs->get_data_type() == rhs->get_data_type();
}


/*********************************************************************************************************************************/

UMinus_Ast::UMinus_Ast(Ast * l, Ast * r, int line){
	lhs = l;
	rhs = r;
	lineno = line;
}

void UMinus_Ast::print(ostream & file_buffer){
	file_buffer<<"\n"<<AST_NODE_SPACE<<"Arith: UMINUS\n";
	file_buffer<<AST_SUB_NODE_SPACE<<"LHS (";
	lhs->print(file_buffer);
	file_buffer<<")";
}

/*********************************************************************************************************************************/

Plus_Ast::Plus_Ast(Ast * l, Ast * r, int line){
	lhs = l;
	rhs = r;
	lineno = line;
}

void Plus_Ast::print(ostream & file_buffer){
	file_buffer<<"\n"<<AST_NODE_SPACE<<"Arith: PLUS\n";
	file_buffer<<AST_SUB_NODE_SPACE<<"LHS (";
	lhs->print(file_buffer);
	file_buffer<<")\n"<<AST_SUB_NODE_SPACE<<"RHS (";
	rhs->print(file_buffer);
	file_buffer<<")";
}	

/*********************************************************************************************************************************/

Minus_Ast::Minus_Ast(Ast * l, Ast * r, int line){
	lhs = l;
	rhs = r;
	lineno = line;
}

void Minus_Ast::print(ostream & file_buffer){
	file_buffer<<"\n"<<AST_NODE_SPACE<<"Arith: MINUS\n";
	file_buffer<<AST_SUB_NODE_SPACE<<"LHS (";
	lhs->print(file_buffer);
	file_buffer<<")\n"<<AST_SUB_NODE_SPACE<<"RHS (";
	rhs->print(file_buffer);
	file_buffer<<")";
}


/*********************************************************************************************************************************/

Divide_Ast::Divide_Ast(Ast * l, Ast * r, int line){
	lhs = l;
	rhs = r;
	lineno = line;
}

void Divide_Ast::print(ostream & file_buffer){
	file_buffer<<"\n"<<AST_NODE_SPACE<<"Arith: DIV\n";
	file_buffer<<AST_SUB_NODE_SPACE<<"LHS (";
	lhs->print(file_buffer);
	file_buffer<<")\n"<<AST_SUB_NODE_SPACE<<"RHS (";
	rhs->print(file_buffer);
	file_buffer<<")";
}


/*********************************************************************************************************************************/

Mult_Ast::Mult_Ast(Ast * l, Ast * r, int line){
	lhs = l;
	rhs = r;
	lineno = line;
}

void Mult_Ast::print(ostream & file_buffer){
	file_buffer<<"\n"<<AST_NODE_SPACE<<"Arith: MULT\n";
	file_buffer<<AST_SUB_NODE_SPACE<<"LHS (";
	lhs->print(file_buffer);
	file_buffer<<")\n"<<AST_SUB_NODE_SPACE<<"RHS (";
	rhs->print(file_buffer);
	file_buffer<<")";
}

/*********************************************************************************************************************************/

Return_Ast::Return_Ast(Ast * ret_val, string name, int line){
	lineno =line;
	proc_name = name;
	return_value = ret_val;
}

Data_Type Return_Ast::get_data_type(){
	
}


void Return_Ast::print(ostream & file_buffer){
	file_buffer<<"fucker fuck here\n";
}



/*********************************************************************************************************************************/

Conditional_Expression_Ast::Conditional_Expression_Ast(Ast* scond, Ast* l, Ast* r, int line){
	cond = scond;
	lhs = l;
	rhs = r;
	lineno = line;
}

void Conditional_Expression_Ast::print(ostream & file_buffer){
	file_buffer<<"\n"<<AST_SPACE<<"Cond:\n";
	file_buffer<<AST_NODE_SPACE<<"IF_ELSE";
	cond->print(file_buffer);
	file_buffer<<"\n"<<AST_NODE_SPACE<<"LHS (";
	lhs->print(file_buffer);
	file_buffer<<")\n"<<AST_NODE_SPACE<<"RHS (";
	rhs->print(file_buffer);
	file_buffer<<")";
}

/*********************************************************************************************************************************/

Relational_Expr_Ast::Relational_Expr_Ast(Ast * lhs, Relational_Op rop, Ast * rhs, int line){
	lhs_condition = lhs;
	rhs_condition = rhs;
	rel_op = rop;
	lineno = line;
}

Relational_Expr_Ast::~Relational_Expr_Ast(){

}

bool Relational_Expr_Ast::check_ast(){
	return lhs_condition->get_data_type() == rhs_condition->get_data_type();
}

void Relational_Expr_Ast::set_data_type(Data_Type dt){
	node_data_type = dt;
}

Data_Type Relational_Expr_Ast::get_data_type(){
	return node_data_type;
}

void Relational_Expr_Ast::print(ostream & file_buffer){
	file_buffer<<"\n"<<AST_NODE_SPACE<<"Condition: ";
	if(rel_op==less_equalto) file_buffer<<"LE\n";
	else if(rel_op==less_than) file_buffer<<"LT\n";
	else if(rel_op==greater_than) file_buffer<<"GT\n";
	else if(rel_op==greater_equalto) file_buffer<<"GE\n";
	else if(rel_op==equalto) file_buffer<<"EQ\n";
	else if(rel_op==not_equalto) file_buffer<<"NE\n";
	file_buffer<<AST_SUB_NODE_SPACE<<"LHS (";
	lhs_condition->print(file_buffer);
	file_buffer<<")\n"<<AST_SUB_NODE_SPACE<<"RHS (";
	rhs_condition->print(file_buffer);
	file_buffer<<")";
}


/*********************************************************************************************************************************/

Logical_Expr_Ast::Logical_Expr_Ast(Ast * lhs, Logical_Op bop, Ast * rhs, int line){
	lhs_op = lhs;
	rhs_op = rhs;
	bool_op = bop;
	lineno = line;
}

Logical_Expr_Ast::~Logical_Expr_Ast(){

}

bool Logical_Expr_Ast::check_ast(){
	;
}

void Logical_Expr_Ast::set_data_type(Data_Type dt){
	node_data_type = dt;
}

Data_Type Logical_Expr_Ast::get_data_type(){
	return node_data_type;
}

void Logical_Expr_Ast::print(ostream & file_buffer){
	file_buffer<<"\n"<<AST_NODE_SPACE<<"Condition: ";
	int k = 0;
	if(bool_op==_logical_not) {
		file_buffer<<"NOT\n";
		k = 1;
	}
	else if(bool_op==_logical_or) file_buffer<<"OR\n";
	else if(bool_op==_logical_and) file_buffer<<"AND\n";
	if(k){
		file_buffer<<AST_SUB_NODE_SPACE<<"RHS (";
		rhs_op->print(file_buffer);
		file_buffer<<")";
	}
	file_buffer<<AST_SUB_NODE_SPACE<<"LHS (";
	lhs_op->print(file_buffer);
	file_buffer<<")\n"<<AST_SUB_NODE_SPACE<<"RHS (";
	rhs_op->print(file_buffer);
	file_buffer<<")";
}


/*********************************************************************************************************************************/


Selection_Statement_Ast::Selection_Statement_Ast(Ast * scond,Ast* sthen_part, Ast* selse_part, int line){
	cond = scond;
	then_part = sthen_part;
	else_part = selse_part;
	lineno = line;
}

Selection_Statement_Ast::~Selection_Statement_Ast(){

}

bool Selection_Statement_Ast::check_ast(){

}

void Selection_Statement_Ast::set_data_type(Data_Type dt){
	node_data_type = dt;
}

Data_Type Selection_Statement_Ast::get_data_type(){
	return node_data_type;
}

void Selection_Statement_Ast::print(ostream & file_buffer){
	file_buffer<<"\n"<<AST_SPACE<<"IF : \n";
	file_buffer<<AST_SPACE<<"CONDITION (";
	cond->print(file_buffer);
	file_buffer<<")\n"<<AST_SPACE<<"THEN (";
	then_part->print(file_buffer);
	file_buffer<<")";
	if(else_part!=NULL){
		file_buffer<<"\n"<<AST_SPACE<<"ELSE (";
		else_part->print(file_buffer);
		file_buffer<<")";
	}
}

/*********************************************************************************************************************************/

Iteration_Statement_Ast::Iteration_Statement_Ast(Ast * scond, Ast* sbody, int line, bool do_form){
	cond = scond;
	is_do_form = do_form;
	body = sbody;
	lineno = line;
}

Iteration_Statement_Ast::~Iteration_Statement_Ast(){

}

bool Iteration_Statement_Ast::check_ast(){

}

void Iteration_Statement_Ast::set_data_type(Data_Type dt){
	node_data_type = dt;
}

Data_Type Iteration_Statement_Ast::get_data_type(){
	return node_data_type;
}

void Iteration_Statement_Ast::print(ostream & file_buffer){
	if(is_do_form){
		file_buffer<<"\n"<<AST_SPACE<<"DO (";
		body->print(file_buffer);
		file_buffer<<")\n"<<AST_SPACE<<"WHILE CONDITION (";
		cond->print(file_buffer);
		file_buffer<<")";
	}
	else{
		file_buffer<<"\n"<<AST_SPACE<<"WHILE : \n";
		file_buffer<<AST_SPACE<<"CONDITION (";
		cond->print(file_buffer);
		file_buffer<<")\n"<<AST_SPACE<<"BODY (";
		body->print(file_buffer);
		file_buffer<<")";
	}
}

/*********************************************************************************************************************************/

Sequence_Ast::Sequence_Ast(int line){

}

void Sequence_Ast::ast_push_back(Ast * ast){
	statement_list.push_back(ast);
}

void Sequence_Ast::print(ostream & file_buffer){
	for (list<Ast* >::iterator it=statement_list.begin(); it != statement_list.end(); ++it){
		file_buffer<<"\n"<<AST_NODE_SPACE;
		(*it)->print(file_buffer);
	}
}


/*********************************************************************************************************************************/

Print_Ast::Print_Ast(Ast *v, int line){
	var = v;
	lineno = line;
}

void Print_Ast::print(ostream & file_buffer){
	file_buffer<<"\n"<<AST_SPACE<<"Print :\n";
	file_buffer<<AST_SUB_NODE_SPACE;
	var->print(file_buffer);
}

/*********************************************************************************************************************************/


void Program::set_procedure(Procedure * proc, int line){
	
}

/*********************************************************************************************************************************/

Call_Ast::Call_Ast(string name, int line){
	procedure_name = name;
	lineno = line;
}

Data_Type Call_Ast::get_data_type(){
	return node_data_type;
}

void Call_Ast::set_register(Register_Descriptor * reg){

}

void Call_Ast::check_actual_formal_param(Symbol_Table & formal_param_list){
	int paramsize = actual_param_list.size();
	list<Symbol_Table_Entry *> & params = formal_param_list.get_table();
	if(params.size()!=paramsize){
		printf("cs316: Error\n");
		exit(0);
	}

	list<Ast* >::iterator ti = actual_param_list.begin();

	for (list<Symbol_Table_Entry* >::iterator it=params.begin(); it != params.end(); ++it){
		if((*it)->get_data_type()!=(*ti)->get_data_type()){
			printf("cs316: Error\n");
			exit(0);
		}
		ti++;
	}
	
}

void Call_Ast::set_actual_param_list(list<Ast *> & param_list){
	for (list<Ast* >::iterator it=param_list.begin(); it != param_list.end(); ++it){
		actual_param_list.push_back(*it);
	}
}

void Call_Ast::print(ostream & file_buffer){

}

Eval_Result & Call_Ast::evaluate(Local_Environment & eval_env, ostream & file_buffer){

}

Code_For_Ast & Call_Ast::compile(){
	Code_For_Ast* res = new Code_For_Ast();
	Procedure* pro = program_object.get_procedure_prototype(procedure_name);

	list<Symbol_Table_Entry *> & params = pro->get_formal_param_list().get_table();
	list<Ast* >::iterator ti = actual_param_list.begin();
	int x = pro->get_formal_param_list().get_start_offset_of_first_symbol();
	int size = pro->get_formal_param_list().get_size();


	pro->get_formal_param_list().set_start_offset_of_first_symbol(-size+x);
	pro->get_formal_param_list().assign_offsets();

	for (list<Symbol_Table_Entry* >::iterator it=params.begin(); it != params.end(); ++it){
		Code_For_Ast& temp = (*ti)->compile();

		for(list<Icode_Stmt *>::iterator itr = temp.get_icode_list().begin();itr!=temp.get_icode_list().end();itr++){
			res->append_ics(*(*itr));	
		}

		Move_IC_Stmt* stmt;
		Symbol_Table_Entry* temp1 = new Symbol_Table_Entry(*(new string()),int_data_type,0,sp_ref);
		temp1->set_symbol_scope(formal);
		temp1->set_start_offset((*it)->get_start_offset());

		if((*it)->get_data_type()==int_data_type)
			stmt = new Move_IC_Stmt(store,new Register_Addr_Opd(temp.get_reg()),new Mem_Addr_Opd(*(temp1)));
		else if((*it)->get_data_type()==double_data_type)
			stmt = new Move_IC_Stmt(store_d,new Register_Addr_Opd(temp.get_reg()),new Mem_Addr_Opd(*(temp1)));

		temp.get_reg()->reset_register_occupied();

		res->append_ics(*stmt);
		
		ti++;
	}

	pro->get_formal_param_list().set_start_offset_of_first_symbol(x);
	pro->get_formal_param_list().assign_offsets();

	Compute_IC_Stmt* stmt;
	if(size-x > 0){
		stmt = new Compute_IC_Stmt(sub,new Register_Addr_Opd(machine_desc_object.spim_register_table[sp]),new Const_Opd<int>(size-x),new Register_Addr_Opd(machine_desc_object.spim_register_table[sp]));
		res->append_ics(*stmt);
	}

	Label_IC_Stmt* jump = new Label_IC_Stmt(jal,procedure_name);
	res->append_ics(*jump);

	if(size-x > 0){
		stmt = new Compute_IC_Stmt(add,new Register_Addr_Opd(machine_desc_object.spim_register_table[sp]),new Const_Opd<int>(size-x),new Register_Addr_Opd(machine_desc_object.spim_register_table[sp]));
		res->append_ics(*stmt);
	}

	Move_IC_Stmt* stmt1;

	if(node_data_type==int_data_type){
		stmt1 = new Move_IC_Stmt(mov,new Register_Addr_Opd(machine_desc_object.spim_register_table[v1]),new Register_Addr_Opd(machine_desc_object.get_new_register<int_reg>()));
		res->append_ics(*stmt1);
		res->set_reg(stmt1->get_result()->get_reg());
	}
	else if(node_data_type==double_data_type){
		stmt1 = new Move_IC_Stmt(move_d,new Register_Addr_Opd(machine_desc_object.spim_register_table[f0]),new Register_Addr_Opd(machine_desc_object.get_new_register<float_reg>()));
		res->append_ics(*stmt1);
		res->set_reg(stmt1->get_result()->get_reg());
	}
	

	return *res;
	
}

Code_For_Ast & Call_Ast::compile_and_optimize_ast(Lra_Outcome & lra){

}
