%{
#include <stdio.h>
#include <vector>
using namespace std;
extern "C" void yyerror(char* s);
extern int yylex(void);
extern int yylineno;

Symbol_Table * gtable;
Symbol_Table * ltable;
Symbol_Table * ptable;
string* pname;

%}


%union{
	int integer_value;
	double double_value;
	std::string * string_value;
	list<Ast *> * ast_list;
	vector<string*> *name_list;
	vector<Symbol_Table_Entry*> *entry_list;
	Ast * ast;
	Symbol_Table * symbol_table;
	Symbol_Table_Entry * symbol_entry;
	Basic_Block * basic_block;
	Procedure * procedure;
}

%left LESS_THAN LESS_THAN_EQUAL GREATER_THAN GREATER_THAN_EQUAL EQUAL NOT_EQUAL
%left '+' '-' OR
%left '*' '/' AND
%left UMINUS NOT


%token UMINUS
%token VOID 
%token INTEGER
%token FLOAT
%token PRINT
%token WHILE
%token DO
%token RETURN
%token IF
%token ELSE
%token LESS_THAN
%token LESS_THAN_EQUAL
%token GREATER_THAN
%token GREATER_THAN_EQUAL
%token EQUAL
%token NOT_EQUAL
%token NOT
%token OR
%token AND
%token<string_value>NAME
%token<integer_value> INTEGER_NUMBER
%token<double_value> DOUBLE_NUMBER
%token ASSIGN


%type<procedure> procedure_definition function_name_params procedure_declaration procedure_definitions
%type<symbol_entry> param eparam
%type<name_list> variable_list 
%type<entry_list> declaration
%type<entry_list> variable_declaration variable_declaration_list 
%type<symbol_table> optional_variable_declaration_list llist declaration_list params eparams program
%type<ast_list> statement_list arguments
%type<ast> statement variable expression constant boolexp relexp block else

%%



program			:	declaration_list 
				procedure_definitions{
					program_object.called_proc_are_defined_check();
				}
			;



declaration_list	:	{
					Symbol_Table * symbol_table = new Symbol_Table();
					symbol_table->set_table_scope(global);
					$$=symbol_table;
					gtable = $$;
				}
			|		declaration_list variable_declaration {
					Symbol_Table * symbol_table = $1;
					vector<Symbol_Table_Entry*>& entries = *$2;
					for(int i=0;i<entries.size();i++){
						entries[i]->set_symbol_scope(global);
						if(symbol_table->variable_in_symbol_list_check(entries[i]->get_variable_name())){
							printf("cs316: Error");
							exit(0);
						}
						symbol_table->push_symbol(entries[i]);
					}
					program_object.set_global_table(*$$);

				}
			|	declaration_list procedure_declaration	
			;

procedure_declaration	:	VOID NAME '(' params ')' ';' {
					if(program_object.is_procedure_exists(*$2)){
						printf("cs316: Error\n");
						exit(0);
					}
					Procedure* pro = new Procedure(void_data_type,*$2,yylineno);
					$$ = pro;
					program_object.set_proc_to_map(*$2,$$);
					pro->set_formal_param_list(*$4);
				}
			|
				INTEGER NAME '(' params ')' ';' {
					if(program_object.is_procedure_exists(*$2)){
						printf("cs316: Error\n");
						exit(0);
					}
					Procedure* pro = new Procedure(int_data_type,*$2,yylineno);
					$$ = pro;
					program_object.set_proc_to_map(*$2,$$);
					pro->set_formal_param_list(*$4);
				}
			|
				FLOAT NAME '(' params ')' ';' {
					if(program_object.is_procedure_exists(*$2)){
						printf("cs316: Error\n");
						exit(0);
					}
					Procedure* pro = new Procedure(double_data_type,*$2,yylineno);
					$$ = pro;
					program_object.set_proc_to_map(*$2,$$);
					pro->set_formal_param_list(*$4);
				}
			|	
				VOID NAME '(' eparams ')' ';' {
					if(program_object.is_procedure_exists(*$2)){
						printf("cs316: Error\n");
						exit(0);
					}
					Procedure* pro = new Procedure(void_data_type,*$2,yylineno);
					$$ = pro;
					program_object.set_proc_to_map(*$2,$$);
					pro->set_formal_param_list(*$4);
				}
			|
				INTEGER NAME '(' eparams ')' ';' {
					if(program_object.is_procedure_exists(*$2)){
						printf("cs316: Error\n");
						exit(0);
					}
					Procedure* pro = new Procedure(int_data_type,*$2,yylineno);
					$$ = pro;
					program_object.set_proc_to_map(*$2,$$);
					pro->set_formal_param_list(*$4);
				}
			|
				FLOAT NAME '(' eparams ')' ';' {
					if(program_object.is_procedure_exists(*$2)){
						printf("cs316: Error\n");
						exit(0);
					}
					Procedure* pro = new Procedure(double_data_type,*$2,yylineno);
					$$ = pro;
					program_object.set_proc_to_map(*$2,$$);
					pro->set_formal_param_list(*$4);
				}
			;



eparams		:	eparam {
				Symbol_Table * symbol_table = new Symbol_Table();
				$$=symbol_table;
				symbol_table->push_symbol($1);
			}	
		|	eparams ',' eparam {
				Symbol_Table * symbol_table = $1;
				symbol_table->push_symbol($3);
			}
		;


eparam		:	INTEGER {
				string* empty = new string();
				$$ = new Symbol_Table_Entry(*empty,int_data_type,yylineno);
			}
		|	FLOAT {
				string* empty = new string();
				$$ = new Symbol_Table_Entry(*empty,double_data_type,yylineno);
			}
		|	VOID {
				string* empty = new string();
				$$ = new Symbol_Table_Entry(*empty,void_data_type,yylineno);
			}
		;

params		:	{
				Symbol_Table * symbol_table = new Symbol_Table();
				$$=symbol_table;
			}
		|	param {
				Symbol_Table * symbol_table = new Symbol_Table();
				$$=symbol_table;
				$1->set_symbol_scope(local);
				symbol_table->push_symbol($1);
				ptable = $$;
			}
		|	params ',' param {
				Symbol_Table * symbol_table = $1;
				$3->set_symbol_scope(local);
				if(symbol_table->variable_in_symbol_list_check($3->get_variable_name())){
					printf("cs316: Error");
					exit(0);
				}
				symbol_table->push_symbol($3);
			}
		;

param 		:	INTEGER NAME {
				$$ = new Symbol_Table_Entry(*$2,int_data_type,yylineno);
			}
		|	FLOAT NAME {
				$$ = new Symbol_Table_Entry(*$2,double_data_type,yylineno);
			}
		|	VOID NAME {
				$$ = new Symbol_Table_Entry(*$2,void_data_type,yylineno);
			}
		;


procedure_definitions	:	procedure_definition
			|	procedure_definitions procedure_definition

procedure_definition	:	function_name_params
	          	   		'{'
						llist
	                    			statement_list
		           		'}'
		           		{

		           			Procedure* pro = $1;
		           			pro->set_local_list(*$3);
		           			pro->set_ast_list(*$4);
		           			$$ = pro;
		           		}
	                 	;

function_name_params	:	VOID NAME '(' params ')'{
					pname = $2;
					if(program_object.is_procedure_exists(*$2)){
						Procedure* pro = program_object.get_procedure_prototype(*$2);
						if(pro->is_proc_defined()){
							printf("cs316: Error\n");
							exit(0);
						}
						$$ = pro;
						pro->set_proc_is_defined();
						pro->set_formal_param_list(*$4);
					}else{
						Procedure* pro = new Procedure(void_data_type,*$2,yylineno);
						$$ = pro;
						pro->set_proc_is_defined();
						program_object.set_proc_to_map(*$2,$$);
						pro->set_formal_param_list(*$4);
					}
				}
			|	
				INTEGER NAME '(' params ')'{
					pname = $2;
					if(program_object.is_procedure_exists(*$2)){
						Procedure* pro = program_object.get_procedure_prototype(*$2);
						if(pro->is_proc_defined()){
							printf("cs316: Error\n");
							exit(0);
						}
						$$ = pro;
						pro->set_proc_is_defined();
						pro->set_formal_param_list(*$4);
					}else{
						Procedure* pro = new Procedure(int_data_type,*$2,yylineno);
						$$ = pro;
						pro->set_proc_is_defined();
						program_object.set_proc_to_map(*$2,$$);
						pro->set_formal_param_list(*$4);
					}
				}
			|
				FLOAT NAME '(' params ')'{
					pname = $2;
					if(program_object.is_procedure_exists(*$2)){
						Procedure* pro = program_object.get_procedure_prototype(*$2);
						if(pro->is_proc_defined()){
							printf("cs316: Error\n");
							exit(0);
						}
						$$ = pro;
						pro->set_proc_is_defined();
						pro->set_formal_param_list(*$4);
					}else{
						Procedure* pro = new Procedure(double_data_type,*$2,yylineno);
						$$ = pro;
						pro->set_proc_is_defined();
						program_object.set_proc_to_map(*$2,$$);
						pro->set_formal_param_list(*$4);
					}	
				}
			;


llist	:	optional_variable_declaration_list{
			ltable = $1;
			ltable->set_table_scope(local);
		}
	;

optional_variable_declaration_list	:	/* empty */
						{
							Symbol_Table * symbol_table = new Symbol_Table();
							$$=symbol_table;
						}
						|	variable_declaration_list{
							Symbol_Table * symbol_table = new Symbol_Table();
							$$=symbol_table;

							vector<Symbol_Table_Entry*>& entries = *$1;

							for(int i=0;i<entries.size();i++){
								entries[i]->set_symbol_scope(local);
								if(symbol_table->variable_in_symbol_list_check(entries[i]->get_variable_name())){
									printf("cs316: Error");
									exit(0);
								}
								symbol_table->push_symbol(entries[i]);
							}
						}
						;

variable_declaration_list		:	variable_declaration{
							vector<Symbol_Table_Entry*>* symbol_table = new vector<Symbol_Table_Entry*>();
							vector<Symbol_Table_Entry*>& sym_list = * $1;
							for(int i=0;i<sym_list.size();i++){
								symbol_table->push_back(sym_list[i]);
							}
							$$ = symbol_table;
						}
					|	variable_declaration_list 
						variable_declaration{
								vector<Symbol_Table_Entry*>* symbol_table = $1;
									vector<Symbol_Table_Entry*>& sym_list =* $2;
								for(int i=0;i<sym_list.size();i++){
									symbol_table->push_back(sym_list[i]);
								}
								$$ = symbol_table;
						}
					;

variable_declaration			:	declaration ';'
					;

declaration		:	INTEGER variable_list{
						vector<string*>& slist = * $2;
						vector<Symbol_Table_Entry*>* sym_list = new vector<Symbol_Table_Entry*>();
						for(int i=0;i<slist.size();i++){
							sym_list->push_back(new Symbol_Table_Entry(*slist[slist.size() -1-i],int_data_type,yylineno));
						}
						$$=sym_list;
					}
			|	FLOAT variable_list{
						vector<string*>& slist = *$2;
						vector<Symbol_Table_Entry*>* sym_list = new vector<Symbol_Table_Entry*>();
						for(int i=0;i<slist.size();i++){
							sym_list->push_back(new Symbol_Table_Entry(*slist[slist.size() -1-i],double_data_type,yylineno));
						}
						$$=sym_list;
					}
			|
				VOID variable_list{
						vector<string*>& slist = *$2;
						vector<Symbol_Table_Entry*>* sym_list = new vector<Symbol_Table_Entry*>();
						for(int i=0;i<slist.size();i++){
							sym_list->push_back(new Symbol_Table_Entry(*slist[slist.size() -1-i],double_data_type,yylineno));
						}
						$$=sym_list;
					}
                        ;

variable_list       :       NAME{
					vector<string*>* slist = new vector<string*>();
					slist->push_back($1);
					$$= slist; 
				}
                    |       variable_list ',' NAME{
	                    	vector<string*>* slist = $1;
	                    	slist->push_back($3);
                    }
					;


statement_list	:	{
				list<Ast *>* alist = new list<Ast*>();
				$$=alist;	
			}
			|	statement_list
				statement{
					list<Ast *>& alist = *$1;
					alist.push_back($2);
				}
			;

statement	:	variable ASSIGN expression ';'{
					Assignment_Ast * ast= new Assignment_Ast($1,$3,yylineno);
					if($1->get_data_type() != $3->get_data_type()){
						printf("cs316: Error\n");
						exit(0);
					}
					$$ = ast;
			}
			|
			WHILE '(' boolexp ')' block {
				Iteration_Statement_Ast* ast = new 
				Iteration_Statement_Ast($3, $5, yylineno, 0);
				$$= ast;
			}
			|
			DO block WHILE '(' boolexp ')' ';' {
				Iteration_Statement_Ast* ast = new 
				Iteration_Statement_Ast($5, $2, yylineno, 1);
				$$= ast;
			}
			|
			IF '(' boolexp')' block else {
				Selection_Statement_Ast* ast = new 
				Selection_Statement_Ast($3 ,$5 , $6, yylineno);
				$$ = ast;
			}
			|
			PRINT variable ';' {
				Print_Ast* ast = new Print_Ast($2, yylineno);
				$$ = ast;
			}
			|
			RETURN expression ';' {
				Return_Ast* ast = new Return_Ast($2, *pname, yylineno);
				ast->set_data_type($2->get_data_type());
				$$ = ast;
			}
			|
			NAME '(' arguments ')' ';' {
  				if(!program_object.is_procedure_exists(*$1)){
  					printf("cs316: Error\n");
  					exit(0);
  				}
  				Procedure* pro = program_object.get_procedure_prototype(*$1);
  				pro->set_proc_is_called();
  				Call_Ast* ast = new Call_Ast(*$1,yylineno);
				ast->set_actual_param_list(*$3);
				ast->check_actual_formal_param(pro->get_formal_param_list());
				$$ = ast;
				//ast->set_data_type(pro->get_return_type());
  		} 
			;

block 		:	'{' statement_list '}'{

				list<Ast *>& alist = *$2;
				
				Sequence_Ast* ast = new Sequence_Ast(yylineno);
				for (list<Ast* >::iterator it=alist.begin(); it != alist.end(); ++it){
					ast->ast_push_back(*it);
				}
				$$= ast;
			

			}
			| 
			statement{
				Sequence_Ast* ast = new Sequence_Ast(yylineno);
				ast->ast_push_back($1);
				$$ = ast;
			}
			;

else		:	{$$ = NULL;}
			|
			ELSE block{
			$$ = $2;
			}
			;

boolexp		:	'(' boolexp ')' {$$ = $2;}
			|
			NOT boolexp {
				Logical_Expr_Ast* ast =  new 
				Logical_Expr_Ast($2, _logical_not, NULL, yylineno);
				ast->set_data_type($2->get_data_type());
				$$ = ast;
			}
			|
			boolexp AND relexp{
				Logical_Expr_Ast* ast =  new 
				Logical_Expr_Ast($1, _logical_and, $3, yylineno);
				if($1->get_data_type() == $3->get_data_type())
	  				ast->set_data_type($1->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
				$$ = ast;
			}
			|
			boolexp OR relexp{
				Logical_Expr_Ast* ast =  new 
				Logical_Expr_Ast($1, _logical_or, $3, yylineno);
				if($1->get_data_type() == $3->get_data_type())
	  				ast->set_data_type($1->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
				$$ = ast;
			}
			|
			relexp
			;

relexp		:	expression LESS_THAN expression{
				Relational_Expr_Ast* ast = new 
				Relational_Expr_Ast($1, less_than,$3, yylineno);
				if($1->get_data_type() == $3->get_data_type())
	  				ast->set_data_type($1->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			$$ = ast;

			}
			|
			expression GREATER_THAN expression{
				Relational_Expr_Ast* ast = new 
				Relational_Expr_Ast($1, greater_than,$3, yylineno);
				if($1->get_data_type() == $3->get_data_type())
	  				ast->set_data_type($1->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			$$ = ast;
			}
			|
			expression LESS_THAN_EQUAL expression{
				Relational_Expr_Ast* ast = new 
				Relational_Expr_Ast($1, less_equalto,$3, yylineno);
				if($1->get_data_type() == $3->get_data_type())
	  				ast->set_data_type($1->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			$$ = ast;
			}
			|
			expression GREATER_THAN_EQUAL expression{
				Relational_Expr_Ast* ast = new 
				Relational_Expr_Ast($1, greater_equalto,$3, yylineno);
				if($1->get_data_type() == $3->get_data_type())
	  				ast->set_data_type($1->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			$$ = ast;
			}
			|
			expression NOT_EQUAL expression{
				Relational_Expr_Ast* ast = new 
				Relational_Expr_Ast($1, not_equalto,$3, yylineno);
				if($1->get_data_type() == $3->get_data_type())
	  				ast->set_data_type($1->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			$$ = ast;
			}
			|
			expression EQUAL expression{
				Relational_Expr_Ast* ast = new 
				Relational_Expr_Ast($1, equalto,$3, yylineno);
				if($1->get_data_type() == $3->get_data_type()){	  				
				ast->set_data_type($1->get_data_type());
				}
	  			else{printf("cs316: Error\n");exit(0);}
	  			$$ = ast;
			}
			;

	
expression 	:	variable
			|	constant    
			|	'(' expression ')' {$$ = $2;}
			|	'-' expression %prec UMINUS{
				UMinus_Ast* ast = new UMinus_Ast($2,NULL,yylineno);
	  			ast->set_data_type($2->get_data_type());
	  			$$ = ast;
			}
			|
			boolexp '?' expression ':' expression {
				Conditional_Expression_Ast* ast = new 
				Conditional_Expression_Ast($1, $3, $5, yylineno);
				if($3->get_data_type() == $5->get_data_type())
	  				ast->set_data_type($3->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			$$ = ast;

			}
	  		|	expression '+' expression {
	  			Plus_Ast* ast = new Plus_Ast($1,$3,yylineno);
	  			if($1->get_data_type() == $3->get_data_type())
	  				ast->set_data_type($1->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			$$ = ast;
	  		}  
	  		|	expression '-' expression {
	  			Minus_Ast* ast = new Minus_Ast($1,$3,yylineno);
	  			if($1->get_data_type() == $3->get_data_type())
	  				ast->set_data_type($1->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			$$ = ast;
	  		}
	  		|	expression '/' expression {
	  			Divide_Ast* ast = new Divide_Ast($1,$3,yylineno);
	  			if($1->get_data_type() == $3->get_data_type())
	  				ast->set_data_type($1->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			$$ = ast;
	  		}
	  		|	expression '*' expression {
	  			Mult_Ast* ast = new Mult_Ast($1,$3,yylineno);
	  			if($1->get_data_type() == $3->get_data_type())
	  				ast->set_data_type($1->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			$$ = ast;
	  		}
	  		|
	  			NAME '(' arguments ')' {
	  				if(!program_object.is_procedure_exists(*$1)){
	  					printf("cs316: Error\n");
	  					exit(0);
	  				}
	  				Procedure* pro = program_object.get_procedure_prototype(*$1);
	  				pro->set_proc_is_called();
	  				Call_Ast* ast = new Call_Ast(*$1,yylineno);
					ast->set_actual_param_list(*$3);
					ast->check_actual_formal_param(pro->get_formal_param_list());
					$$ = ast;
					ast->set_data_type(pro->get_return_type());
	  			}	
	  		;


arguments	:	{
				list<Ast *>* alist = new list<Ast*>();
				$$ = alist;
			}
		|	expression{
				list<Ast *>* alist = new list<Ast*>();
				alist->push_back($1);
				$$ = alist;
			}
		|	arguments ',' expression{
				list<Ast *>& alist = *$1;
				alist.push_back($3);
			}
		;

variable	:	NAME {
				Name_Ast* ast;

				if(ltable && ltable->variable_in_symbol_list_check(* $1)){
					ast = new Name_Ast(*$1,ltable->get_symbol_table_entry(* $1),yylineno);
				}
				else if(ptable && ptable->variable_in_symbol_list_check(*$1)){
					ast = new Name_Ast(*$1,ptable->get_symbol_table_entry(* $1),yylineno);
				}
				else if(gtable && gtable->variable_in_symbol_list_check(*$1)){
					ast = new Name_Ast(*$1,gtable->get_symbol_table_entry(* $1),yylineno);
				}
				else{
					printf("cs316: Error\n");
					exit(0);
				}
				$$ = ast;
			    }
			;

constant		:	INTEGER_NUMBER{
					Number_Ast<int> *ast = new Number_Ast<int>($1,int_data_type,yylineno); 
					$$ = ast;
				} 
			|
				DOUBLE_NUMBER{
					Number_Ast<double> *ast = new Number_Ast<double>($1,double_data_type,yylineno); 
					$$ = ast;
				}
			;

%%

