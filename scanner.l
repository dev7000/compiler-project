%{
# include "parser.tab.h"
%}

digit [0-9]
letter [a-zA-Z_]
char [0-9a-zA-Z_]
operator [-+*/]
meta [{}(),;:?]
word {letter}{char}*
%%

[ \t\n] ;

"//".* ;

"/*"[^*]*(\*([^/][^*]*)?)*"*/" ;

"int"	{
	store_token_name("INTEGER",yytext,yylineno);				
	return INTEGER;
	}

"void"	{
	store_token_name("VOID",yytext,yylineno);		
	return VOID;	
	}

"float"	{
	store_token_name("FLOAT",yytext,yylineno);
	return FLOAT;	
	}

"if"	{
	store_token_name("if",yytext,yylineno);
	return IF;	
	}

"else"	{
	store_token_name("ELSE",yytext,yylineno);
	return ELSE;	
	}

"while" {
	store_token_name("WHILE",yytext,yylineno);
	return WHILE;	
	}

"print" {
	store_token_name("PRINT",yytext,yylineno);
	return PRINT;
	}

"do"	{
	store_token_name("DO",yytext,yylineno);
	return DO;	
	}

"return"	{
	store_token_name("RETURN",yytext,yylineno);
	return RETURN;	
	}
	
"=="	{
	store_token_name("EQUAL",yytext,yylineno);
	return EQUAL;
	}

"!="	{
	store_token_name("NOT_EQUAL",yytext,yylineno);
	return NOT_EQUAL;
	}


"<"	{
	store_token_name("LESS_THAN",yytext,yylineno);
	return LESS_THAN;
	}

"<="	{
	store_token_name("LESS_THAN_EQUAL",yytext,yylineno);
	return LESS_THAN_EQUAL;
	}

">"	{
	store_token_name("GREATER_THAN",yytext,yylineno);
	return GREATER_THAN;
	}

">="	{
	store_token_name("GREATER_THAN_EQUAL",yytext,yylineno);
	return GREATER_THAN_EQUAL;
	}

"!"	{
	store_token_name("NOT",yytext,yylineno);
	return NOT;
	}

"||"	{
	store_token_name("OR",yytext,yylineno);
	return OR;
	}

"&&"	{
	store_token_name("AND",yytext,yylineno);
	return AND;
	}

{word}	{
	store_token_name("NAME",yytext,yylineno);
	string* str = new string(yytext);
	yylval.string_value = str;		
	return NAME;
	}
	
{digit}+ {
	 store_token_name("NUM",yytext,yylineno);
	 yylval.integer_value = atoi(yytext);
	 return INTEGER_NUMBER; 
	 }		

{digit}+\.{digit}+ {
	 store_token_name("FNUM",yytext,yylineno);
	 yylval.double_value = atof(yytext);
	 return DOUBLE_NUMBER; 
	 }	

{meta}	{
	store_token_name("META CHAR",yytext,yylineno);
	return (int)yytext[0];
	}

=	{
	store_token_name("ASSIGN_OP",yytext,yylineno);	
	return ASSIGN;	
	}

{operator} { 
		store_token_name("ARITHOP",yytext,yylineno);
		return (int)yytext[0];
	   }

.	{
		printf("cs316: Error\n");
		exit(0);
	}



	
