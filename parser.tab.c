/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "parser.y" /* yacc.c:339  */

#include <stdio.h>
#include <vector>
using namespace std;
extern "C" void yyerror(char* s);
extern int yylex(void);
extern int yylineno;

Symbol_Table * gtable;
Symbol_Table * ltable;
Symbol_Table * ptable;
string* pname;


#line 81 "parser.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "parser.tab.h".  */
#ifndef YY_YY_PARSER_TAB_H_INCLUDED
# define YY_YY_PARSER_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    LESS_THAN = 258,
    LESS_THAN_EQUAL = 259,
    GREATER_THAN = 260,
    GREATER_THAN_EQUAL = 261,
    EQUAL = 262,
    NOT_EQUAL = 263,
    OR = 264,
    AND = 265,
    UMINUS = 266,
    NOT = 267,
    VOID = 268,
    INTEGER = 269,
    FLOAT = 270,
    PRINT = 271,
    WHILE = 272,
    DO = 273,
    RETURN = 274,
    IF = 275,
    ELSE = 276,
    NAME = 277,
    INTEGER_NUMBER = 278,
    DOUBLE_NUMBER = 279,
    ASSIGN = 280
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 17 "parser.y" /* yacc.c:355  */

	int integer_value;
	double double_value;
	std::string * string_value;
	list<Ast *> * ast_list;
	vector<string*> *name_list;
	vector<Symbol_Table_Entry*> *entry_list;
	Ast * ast;
	Symbol_Table * symbol_table;
	Symbol_Table_Entry * symbol_entry;
	Basic_Block * basic_block;
	Procedure * procedure;

#line 161 "parser.tab.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_PARSER_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 178 "parser.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  3
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   237

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  38
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  27
/* YYNRULES -- Number of rules.  */
#define YYNRULES  79
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  181

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   280

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      30,    31,    12,     9,    33,    10,     2,    13,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    37,    32,
       2,     2,     2,    36,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    34,     2,    35,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,    11,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    75,    75,    83,    89,   103,   106,   117,   128,   139,
     150,   161,   175,   180,   187,   191,   195,   201,   205,   212,
     223,   226,   229,   235,   236,   238,   252,   272,   292,   314,
     321,   325,   342,   350,   361,   364,   372,   381,   391,   396,
     403,   407,   414,   423,   429,   435,   441,   446,   452,   467,
     480,   487,   489,   494,   496,   503,   512,   521,   524,   534,
     543,   552,   561,   570,   582,   583,   584,   585,   591,   600,
     607,   614,   621,   629,   645,   649,   654,   660,   680,   685
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "LESS_THAN", "LESS_THAN_EQUAL",
  "GREATER_THAN", "GREATER_THAN_EQUAL", "EQUAL", "NOT_EQUAL", "'+'", "'-'",
  "OR", "'*'", "'/'", "AND", "UMINUS", "NOT", "VOID", "INTEGER", "FLOAT",
  "PRINT", "WHILE", "DO", "RETURN", "IF", "ELSE", "NAME", "INTEGER_NUMBER",
  "DOUBLE_NUMBER", "ASSIGN", "'('", "')'", "';'", "','", "'{'", "'}'",
  "'?'", "':'", "$accept", "program", "declaration_list",
  "procedure_declaration", "eparams", "eparam", "params", "param",
  "procedure_definitions", "procedure_definition", "function_name_params",
  "llist", "optional_variable_declaration_list",
  "variable_declaration_list", "variable_declaration", "declaration",
  "variable_list", "statement_list", "statement", "block", "else",
  "boolexp", "relexp", "expression", "arguments", "variable", "constant", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,    43,
      45,   264,    42,    47,   265,   266,   267,   268,   269,   270,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
      40,    41,    59,    44,   123,   125,    63,    58
};
# endif

#define YYPACT_NINF -142

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-142)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
    -142,     5,    -2,  -142,    -6,     0,     3,  -142,    26,  -142,
      41,  -142,    18,    23,    56,    70,    56,    78,    56,    79,
      92,    95,  -142,    80,  -142,   128,   105,   128,   128,   106,
     113,   134,   136,   136,   136,  -142,  -142,    80,  -142,   146,
     157,   171,   -22,  -142,    15,  -142,  -142,    62,   109,   132,
     148,   131,   131,   131,  -142,   147,  -142,  -142,  -142,  -142,
     175,   139,   185,   131,   186,   187,   188,   189,   146,   157,
     171,   160,   161,   179,   197,   192,   164,    76,   194,   195,
    -142,  -142,    65,  -142,  -142,  -142,  -142,  -142,  -142,  -142,
    -142,  -142,  -142,  -142,  -142,  -142,  -142,  -142,   198,    76,
    -142,  -142,   205,    76,    76,   199,  -142,  -142,    76,    16,
    -142,    75,  -142,  -142,    76,    76,    76,  -142,    11,   196,
     154,   201,  -142,   191,    76,   123,   120,    76,    76,    76,
      76,    76,    76,    76,    76,    76,    76,    76,    76,    76,
    -142,   124,   196,   180,   107,   164,  -142,    76,   183,  -142,
    -142,  -142,  -142,    64,   196,   196,   196,   196,   196,   196,
      19,    19,  -142,  -142,   164,   200,    76,  -142,  -142,   130,
    -142,    76,   190,  -142,   196,   202,   196,   164,  -142,  -142,
    -142
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       3,     0,     0,     1,     0,     0,     0,     5,     2,    23,
       0,     4,     0,    38,    37,    38,    35,    38,    36,     0,
       0,     0,    24,    30,    34,    17,     0,    17,    17,     0,
       0,     0,     0,     0,     0,    40,    29,    31,    32,    16,
      14,    15,     0,    12,     0,    18,    39,     0,     0,     0,
       0,    17,    17,    17,    38,     0,    33,    22,    20,    21,
       0,     0,    26,     0,     0,    27,     0,    28,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    77,
      25,    41,     0,     9,    16,    14,    15,    13,     6,    19,
      10,     7,    11,     8,    26,    27,    28,    77,     0,     0,
      40,    50,     0,     0,     0,    77,    78,    79,     0,     0,
      57,     0,    64,    65,     0,    74,     0,    46,     0,     0,
       0,     0,    67,    54,    74,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      47,     0,    75,     0,     0,     0,    49,     0,     0,    53,
      66,    56,    55,     0,    58,    60,    59,    61,    63,    62,
      69,    70,    72,    71,     0,     0,     0,    42,    43,     0,
      73,     0,    51,    48,    76,     0,    68,     0,    45,    44,
      52
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -142,  -142,  -142,  -142,   125,   167,   -14,   170,  -142,   227,
    -142,  -142,  -142,  -142,   -13,  -142,     1,   137,   -54,  -141,
    -142,   -96,    68,   -75,   112,   -55,  -142
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     2,     7,    42,    43,    44,    45,     8,     9,
      10,    35,    36,    37,    11,    12,    14,    55,   101,   102,
     178,   109,   110,   119,   143,   112,   113
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      82,    81,   111,   118,   168,     3,    16,    18,   123,    60,
      38,    61,   125,    48,    50,     4,     5,     6,   141,    98,
      13,    82,   127,   172,    56,   128,    15,   127,   122,    17,
     128,   138,   139,   126,    16,    18,   180,    71,    72,    73,
     142,   144,   145,    19,    20,    21,    62,   129,    63,   142,
      24,   169,   129,    25,   153,   154,   155,   156,   157,   158,
     159,   160,   161,   162,   163,    82,    81,   130,   131,   132,
     133,   134,   135,   136,   137,    23,   138,   139,   130,   131,
     132,   133,   134,   135,   136,   137,   103,   138,   139,    26,
      82,   174,   104,    64,   116,    61,   176,    32,    33,    34,
      27,   171,   105,   106,   107,    29,   108,   140,    28,    82,
     130,   131,   132,   133,   134,   135,   136,   137,    30,   138,
     139,    31,    82,   130,   131,   132,   133,   134,   135,   136,
     137,    46,   138,   139,   127,   127,    51,   128,   128,   167,
      65,   127,    63,    52,   128,    39,    40,    41,    68,    69,
      70,   150,    47,    49,   149,   164,    84,    85,    86,   129,
     129,   175,    54,    66,    53,    61,   129,    74,    75,    76,
      77,    78,    57,    79,    74,    75,    76,    77,    78,    67,
      79,    63,    80,    58,    74,    75,    76,    77,    78,   146,
      79,    94,    95,    63,    63,   151,   152,    59,   100,   130,
     131,   132,   133,   134,   135,   136,   137,    83,   138,   139,
      96,   165,    63,   166,   170,   177,   166,    88,    90,    91,
      92,    93,    99,    97,   114,   115,   121,   129,    87,   124,
     117,   147,   173,    89,   179,    22,   148,   120
};

static const yytype_uint8 yycheck[] =
{
      55,    55,    77,    99,   145,     0,     5,     6,   104,    31,
      23,    33,   108,    27,    28,    17,    18,    19,   114,    74,
      26,    76,    11,   164,    37,    14,    26,    11,   103,    26,
      14,    12,    13,   108,    33,    34,   177,    51,    52,    53,
     115,   116,    31,    17,    18,    19,    31,    36,    33,   124,
      32,   147,    36,    30,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   120,   120,     3,     4,     5,
       6,     7,     8,     9,    10,    34,    12,    13,     3,     4,
       5,     6,     7,     8,     9,    10,    10,    12,    13,    33,
     145,   166,    16,    31,    29,    33,   171,    17,    18,    19,
      30,    37,    26,    27,    28,    26,    30,    32,    30,   164,
       3,     4,     5,     6,     7,     8,     9,    10,    26,    12,
      13,    26,   177,     3,     4,     5,     6,     7,     8,     9,
      10,    26,    12,    13,    11,    11,    30,    14,    14,    32,
      31,    11,    33,    30,    14,    17,    18,    19,    17,    18,
      19,    31,    27,    28,    31,    31,    17,    18,    19,    36,
      36,    31,    26,    31,    30,    33,    36,    20,    21,    22,
      23,    24,    26,    26,    20,    21,    22,    23,    24,    31,
      26,    33,    35,    26,    20,    21,    22,    23,    24,    35,
      26,    31,    31,    33,    33,   127,   128,    26,    34,     3,
       4,     5,     6,     7,     8,     9,    10,    32,    12,    13,
      31,    31,    33,    33,    31,    25,    33,    32,    32,    32,
      32,    32,    30,    26,    30,    30,    21,    36,    61,    30,
      32,    30,    32,    63,    32,     8,   124,   100
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    39,    40,     0,    17,    18,    19,    41,    46,    47,
      48,    52,    53,    26,    54,    26,    54,    26,    54,    17,
      18,    19,    47,    34,    32,    30,    33,    30,    30,    26,
      26,    26,    17,    18,    19,    49,    50,    51,    52,    17,
      18,    19,    42,    43,    44,    45,    26,    42,    44,    42,
      44,    30,    30,    30,    26,    55,    52,    26,    26,    26,
      31,    33,    31,    33,    31,    31,    31,    31,    17,    18,
      19,    44,    44,    44,    20,    21,    22,    23,    24,    26,
      35,    56,    63,    32,    17,    18,    19,    43,    32,    45,
      32,    32,    32,    32,    31,    31,    31,    26,    63,    30,
      34,    56,    57,    10,    16,    26,    27,    28,    30,    59,
      60,    61,    63,    64,    30,    30,    29,    32,    59,    61,
      55,    21,    61,    59,    30,    59,    61,    11,    14,    36,
       3,     4,     5,     6,     7,     8,     9,    10,    12,    13,
      32,    59,    61,    62,    61,    31,    35,    30,    62,    31,
      31,    60,    60,    61,    61,    61,    61,    61,    61,    61,
      61,    61,    61,    61,    31,    31,    33,    32,    57,    59,
      31,    37,    57,    32,    61,    31,    61,    25,    58,    32,
      57
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    38,    39,    40,    40,    40,    41,    41,    41,    41,
      41,    41,    42,    42,    43,    43,    43,    44,    44,    44,
      45,    45,    45,    46,    46,    47,    48,    48,    48,    49,
      50,    50,    51,    51,    52,    53,    53,    53,    54,    54,
      55,    55,    56,    56,    56,    56,    56,    56,    56,    57,
      57,    58,    58,    59,    59,    59,    59,    59,    60,    60,
      60,    60,    60,    60,    61,    61,    61,    61,    61,    61,
      61,    61,    61,    61,    62,    62,    62,    63,    64,    64
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     0,     2,     2,     6,     6,     6,     6,
       6,     6,     1,     3,     1,     1,     1,     0,     1,     3,
       2,     2,     2,     1,     2,     5,     5,     5,     5,     1,
       0,     1,     1,     2,     2,     2,     2,     2,     1,     3,
       0,     2,     4,     5,     7,     6,     3,     3,     5,     3,
       1,     0,     2,     3,     2,     3,     3,     1,     3,     3,
       3,     3,     3,     3,     1,     1,     3,     2,     5,     3,
       3,     3,     3,     4,     0,     1,     3,     1,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 76 "parser.y" /* yacc.c:1646  */
    {
					program_object.called_proc_are_defined_check();
				}
#line 1386 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 3:
#line 83 "parser.y" /* yacc.c:1646  */
    {
					Symbol_Table * symbol_table = new Symbol_Table();
					symbol_table->set_table_scope(global);
					(yyval.symbol_table)=symbol_table;
					gtable = (yyval.symbol_table);
				}
#line 1397 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 4:
#line 89 "parser.y" /* yacc.c:1646  */
    {
					Symbol_Table * symbol_table = (yyvsp[-1].symbol_table);
					vector<Symbol_Table_Entry*>& entries = *(yyvsp[0].entry_list);
					for(int i=0;i<entries.size();i++){
						entries[i]->set_symbol_scope(global);
						if(symbol_table->variable_in_symbol_list_check(entries[i]->get_variable_name())){
							printf("cs316: Error");
							exit(0);
						}
						symbol_table->push_symbol(entries[i]);
					}
					program_object.set_global_table(*(yyval.symbol_table));

				}
#line 1416 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 6:
#line 106 "parser.y" /* yacc.c:1646  */
    {
					if(program_object.is_procedure_exists(*(yyvsp[-4].string_value))){
						printf("cs316: Error\n");
						exit(0);
					}
					Procedure* pro = new Procedure(void_data_type,*(yyvsp[-4].string_value),yylineno);
					(yyval.procedure) = pro;
					program_object.set_proc_to_map(*(yyvsp[-4].string_value),(yyval.procedure));
					pro->set_formal_param_list(*(yyvsp[-2].symbol_table));
				}
#line 1431 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 7:
#line 117 "parser.y" /* yacc.c:1646  */
    {
					if(program_object.is_procedure_exists(*(yyvsp[-4].string_value))){
						printf("cs316: Error\n");
						exit(0);
					}
					Procedure* pro = new Procedure(int_data_type,*(yyvsp[-4].string_value),yylineno);
					(yyval.procedure) = pro;
					program_object.set_proc_to_map(*(yyvsp[-4].string_value),(yyval.procedure));
					pro->set_formal_param_list(*(yyvsp[-2].symbol_table));
				}
#line 1446 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 128 "parser.y" /* yacc.c:1646  */
    {
					if(program_object.is_procedure_exists(*(yyvsp[-4].string_value))){
						printf("cs316: Error\n");
						exit(0);
					}
					Procedure* pro = new Procedure(double_data_type,*(yyvsp[-4].string_value),yylineno);
					(yyval.procedure) = pro;
					program_object.set_proc_to_map(*(yyvsp[-4].string_value),(yyval.procedure));
					pro->set_formal_param_list(*(yyvsp[-2].symbol_table));
				}
#line 1461 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 139 "parser.y" /* yacc.c:1646  */
    {
					if(program_object.is_procedure_exists(*(yyvsp[-4].string_value))){
						printf("cs316: Error\n");
						exit(0);
					}
					Procedure* pro = new Procedure(void_data_type,*(yyvsp[-4].string_value),yylineno);
					(yyval.procedure) = pro;
					program_object.set_proc_to_map(*(yyvsp[-4].string_value),(yyval.procedure));
					pro->set_formal_param_list(*(yyvsp[-2].symbol_table));
				}
#line 1476 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 150 "parser.y" /* yacc.c:1646  */
    {
					if(program_object.is_procedure_exists(*(yyvsp[-4].string_value))){
						printf("cs316: Error\n");
						exit(0);
					}
					Procedure* pro = new Procedure(int_data_type,*(yyvsp[-4].string_value),yylineno);
					(yyval.procedure) = pro;
					program_object.set_proc_to_map(*(yyvsp[-4].string_value),(yyval.procedure));
					pro->set_formal_param_list(*(yyvsp[-2].symbol_table));
				}
#line 1491 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 161 "parser.y" /* yacc.c:1646  */
    {
					if(program_object.is_procedure_exists(*(yyvsp[-4].string_value))){
						printf("cs316: Error\n");
						exit(0);
					}
					Procedure* pro = new Procedure(double_data_type,*(yyvsp[-4].string_value),yylineno);
					(yyval.procedure) = pro;
					program_object.set_proc_to_map(*(yyvsp[-4].string_value),(yyval.procedure));
					pro->set_formal_param_list(*(yyvsp[-2].symbol_table));
				}
#line 1506 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 12:
#line 175 "parser.y" /* yacc.c:1646  */
    {
				Symbol_Table * symbol_table = new Symbol_Table();
				(yyval.symbol_table)=symbol_table;
				symbol_table->push_symbol((yyvsp[0].symbol_entry));
			}
#line 1516 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 180 "parser.y" /* yacc.c:1646  */
    {
				Symbol_Table * symbol_table = (yyvsp[-2].symbol_table);
				symbol_table->push_symbol((yyvsp[0].symbol_entry));
			}
#line 1525 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 187 "parser.y" /* yacc.c:1646  */
    {
				string* empty = new string();
				(yyval.symbol_entry) = new Symbol_Table_Entry(*empty,int_data_type,yylineno);
			}
#line 1534 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 191 "parser.y" /* yacc.c:1646  */
    {
				string* empty = new string();
				(yyval.symbol_entry) = new Symbol_Table_Entry(*empty,double_data_type,yylineno);
			}
#line 1543 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 195 "parser.y" /* yacc.c:1646  */
    {
				string* empty = new string();
				(yyval.symbol_entry) = new Symbol_Table_Entry(*empty,void_data_type,yylineno);
			}
#line 1552 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 201 "parser.y" /* yacc.c:1646  */
    {
				Symbol_Table * symbol_table = new Symbol_Table();
				(yyval.symbol_table)=symbol_table;
			}
#line 1561 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 205 "parser.y" /* yacc.c:1646  */
    {
				Symbol_Table * symbol_table = new Symbol_Table();
				(yyval.symbol_table)=symbol_table;
				(yyvsp[0].symbol_entry)->set_symbol_scope(local);
				symbol_table->push_symbol((yyvsp[0].symbol_entry));
				ptable = (yyval.symbol_table);
			}
#line 1573 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 212 "parser.y" /* yacc.c:1646  */
    {
				Symbol_Table * symbol_table = (yyvsp[-2].symbol_table);
				(yyvsp[0].symbol_entry)->set_symbol_scope(local);
				if(symbol_table->variable_in_symbol_list_check((yyvsp[0].symbol_entry)->get_variable_name())){
					printf("cs316: Error");
					exit(0);
				}
				symbol_table->push_symbol((yyvsp[0].symbol_entry));
			}
#line 1587 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 223 "parser.y" /* yacc.c:1646  */
    {
				(yyval.symbol_entry) = new Symbol_Table_Entry(*(yyvsp[0].string_value),int_data_type,yylineno);
			}
#line 1595 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 226 "parser.y" /* yacc.c:1646  */
    {
				(yyval.symbol_entry) = new Symbol_Table_Entry(*(yyvsp[0].string_value),double_data_type,yylineno);
			}
#line 1603 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 229 "parser.y" /* yacc.c:1646  */
    {
				(yyval.symbol_entry) = new Symbol_Table_Entry(*(yyvsp[0].string_value),void_data_type,yylineno);
			}
#line 1611 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 243 "parser.y" /* yacc.c:1646  */
    {

		           			Procedure* pro = (yyvsp[-4].procedure);
		           			pro->set_local_list(*(yyvsp[-2].symbol_table));
		           			pro->set_ast_list(*(yyvsp[-1].ast_list));
		           			(yyval.procedure) = pro;
		           		}
#line 1623 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 252 "parser.y" /* yacc.c:1646  */
    {
					pname = (yyvsp[-3].string_value);
					if(program_object.is_procedure_exists(*(yyvsp[-3].string_value))){
						Procedure* pro = program_object.get_procedure_prototype(*(yyvsp[-3].string_value));
						if(pro->is_proc_defined()){
							printf("cs316: Error\n");
							exit(0);
						}
						(yyval.procedure) = pro;
						pro->set_proc_is_defined();
						pro->set_formal_param_list(*(yyvsp[-1].symbol_table));
					}else{
						Procedure* pro = new Procedure(void_data_type,*(yyvsp[-3].string_value),yylineno);
						(yyval.procedure) = pro;
						pro->set_proc_is_defined();
						program_object.set_proc_to_map(*(yyvsp[-3].string_value),(yyval.procedure));
						pro->set_formal_param_list(*(yyvsp[-1].symbol_table));
					}
				}
#line 1647 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 272 "parser.y" /* yacc.c:1646  */
    {
					pname = (yyvsp[-3].string_value);
					if(program_object.is_procedure_exists(*(yyvsp[-3].string_value))){
						Procedure* pro = program_object.get_procedure_prototype(*(yyvsp[-3].string_value));
						if(pro->is_proc_defined()){
							printf("cs316: Error\n");
							exit(0);
						}
						(yyval.procedure) = pro;
						pro->set_proc_is_defined();
						pro->set_formal_param_list(*(yyvsp[-1].symbol_table));
					}else{
						Procedure* pro = new Procedure(int_data_type,*(yyvsp[-3].string_value),yylineno);
						(yyval.procedure) = pro;
						pro->set_proc_is_defined();
						program_object.set_proc_to_map(*(yyvsp[-3].string_value),(yyval.procedure));
						pro->set_formal_param_list(*(yyvsp[-1].symbol_table));
					}
				}
#line 1671 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 292 "parser.y" /* yacc.c:1646  */
    {
					pname = (yyvsp[-3].string_value);
					if(program_object.is_procedure_exists(*(yyvsp[-3].string_value))){
						Procedure* pro = program_object.get_procedure_prototype(*(yyvsp[-3].string_value));
						if(pro->is_proc_defined()){
							printf("cs316: Error\n");
							exit(0);
						}
						(yyval.procedure) = pro;
						pro->set_proc_is_defined();
						pro->set_formal_param_list(*(yyvsp[-1].symbol_table));
					}else{
						Procedure* pro = new Procedure(double_data_type,*(yyvsp[-3].string_value),yylineno);
						(yyval.procedure) = pro;
						pro->set_proc_is_defined();
						program_object.set_proc_to_map(*(yyvsp[-3].string_value),(yyval.procedure));
						pro->set_formal_param_list(*(yyvsp[-1].symbol_table));
					}	
				}
#line 1695 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 314 "parser.y" /* yacc.c:1646  */
    {
			ltable = (yyvsp[0].symbol_table);
			ltable->set_table_scope(local);
		}
#line 1704 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 321 "parser.y" /* yacc.c:1646  */
    {
							Symbol_Table * symbol_table = new Symbol_Table();
							(yyval.symbol_table)=symbol_table;
						}
#line 1713 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 325 "parser.y" /* yacc.c:1646  */
    {
							Symbol_Table * symbol_table = new Symbol_Table();
							(yyval.symbol_table)=symbol_table;

							vector<Symbol_Table_Entry*>& entries = *(yyvsp[0].entry_list);

							for(int i=0;i<entries.size();i++){
								entries[i]->set_symbol_scope(local);
								if(symbol_table->variable_in_symbol_list_check(entries[i]->get_variable_name())){
									printf("cs316: Error");
									exit(0);
								}
								symbol_table->push_symbol(entries[i]);
							}
						}
#line 1733 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 342 "parser.y" /* yacc.c:1646  */
    {
							vector<Symbol_Table_Entry*>* symbol_table = new vector<Symbol_Table_Entry*>();
							vector<Symbol_Table_Entry*>& sym_list = * (yyvsp[0].entry_list);
							for(int i=0;i<sym_list.size();i++){
								symbol_table->push_back(sym_list[i]);
							}
							(yyval.entry_list) = symbol_table;
						}
#line 1746 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 351 "parser.y" /* yacc.c:1646  */
    {
								vector<Symbol_Table_Entry*>* symbol_table = (yyvsp[-1].entry_list);
									vector<Symbol_Table_Entry*>& sym_list =* (yyvsp[0].entry_list);
								for(int i=0;i<sym_list.size();i++){
									symbol_table->push_back(sym_list[i]);
								}
								(yyval.entry_list) = symbol_table;
						}
#line 1759 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 364 "parser.y" /* yacc.c:1646  */
    {
						vector<string*>& slist = * (yyvsp[0].name_list);
						vector<Symbol_Table_Entry*>* sym_list = new vector<Symbol_Table_Entry*>();
						for(int i=0;i<slist.size();i++){
							sym_list->push_back(new Symbol_Table_Entry(*slist[slist.size() -1-i],int_data_type,yylineno));
						}
						(yyval.entry_list)=sym_list;
					}
#line 1772 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 372 "parser.y" /* yacc.c:1646  */
    {
						vector<string*>& slist = *(yyvsp[0].name_list);
						vector<Symbol_Table_Entry*>* sym_list = new vector<Symbol_Table_Entry*>();
						for(int i=0;i<slist.size();i++){
							sym_list->push_back(new Symbol_Table_Entry(*slist[slist.size() -1-i],double_data_type,yylineno));
						}
						(yyval.entry_list)=sym_list;
					}
#line 1785 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 381 "parser.y" /* yacc.c:1646  */
    {
						vector<string*>& slist = *(yyvsp[0].name_list);
						vector<Symbol_Table_Entry*>* sym_list = new vector<Symbol_Table_Entry*>();
						for(int i=0;i<slist.size();i++){
							sym_list->push_back(new Symbol_Table_Entry(*slist[slist.size() -1-i],double_data_type,yylineno));
						}
						(yyval.entry_list)=sym_list;
					}
#line 1798 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 391 "parser.y" /* yacc.c:1646  */
    {
					vector<string*>* slist = new vector<string*>();
					slist->push_back((yyvsp[0].string_value));
					(yyval.name_list)= slist; 
				}
#line 1808 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 396 "parser.y" /* yacc.c:1646  */
    {
	                    	vector<string*>* slist = (yyvsp[-2].name_list);
	                    	slist->push_back((yyvsp[0].string_value));
                    }
#line 1817 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 403 "parser.y" /* yacc.c:1646  */
    {
				list<Ast *>* alist = new list<Ast*>();
				(yyval.ast_list)=alist;	
			}
#line 1826 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 408 "parser.y" /* yacc.c:1646  */
    {
					list<Ast *>& alist = *(yyvsp[-1].ast_list);
					alist.push_back((yyvsp[0].ast));
				}
#line 1835 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 414 "parser.y" /* yacc.c:1646  */
    {
					Assignment_Ast * ast= new Assignment_Ast((yyvsp[-3].ast),(yyvsp[-1].ast),yylineno);
					if((yyvsp[-3].ast)->get_data_type() != (yyvsp[-1].ast)->get_data_type()){
						printf("cs316: Error\n");
						exit(0);
					}
					(yyval.ast) = ast;
			}
#line 1848 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 423 "parser.y" /* yacc.c:1646  */
    {
				Iteration_Statement_Ast* ast = new 
				Iteration_Statement_Ast((yyvsp[-2].ast), (yyvsp[0].ast), yylineno, 0);
				(yyval.ast)= ast;
			}
#line 1858 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 429 "parser.y" /* yacc.c:1646  */
    {
				Iteration_Statement_Ast* ast = new 
				Iteration_Statement_Ast((yyvsp[-2].ast), (yyvsp[-5].ast), yylineno, 1);
				(yyval.ast)= ast;
			}
#line 1868 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 435 "parser.y" /* yacc.c:1646  */
    {
				Selection_Statement_Ast* ast = new 
				Selection_Statement_Ast((yyvsp[-3].ast) ,(yyvsp[-1].ast) , (yyvsp[0].ast), yylineno);
				(yyval.ast) = ast;
			}
#line 1878 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 441 "parser.y" /* yacc.c:1646  */
    {
				Print_Ast* ast = new Print_Ast((yyvsp[-1].ast), yylineno);
				(yyval.ast) = ast;
			}
#line 1887 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 446 "parser.y" /* yacc.c:1646  */
    {
				Return_Ast* ast = new Return_Ast((yyvsp[-1].ast), *pname, yylineno);
				ast->set_data_type((yyvsp[-1].ast)->get_data_type());
				(yyval.ast) = ast;
			}
#line 1897 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 452 "parser.y" /* yacc.c:1646  */
    {
  				if(!program_object.is_procedure_exists(*(yyvsp[-4].string_value))){
  					printf("cs316: Error\n");
  					exit(0);
  				}
  				Procedure* pro = program_object.get_procedure_prototype(*(yyvsp[-4].string_value));
  				pro->set_proc_is_called();
  				Call_Ast* ast = new Call_Ast(*(yyvsp[-4].string_value),yylineno);
				ast->set_actual_param_list(*(yyvsp[-2].ast_list));
				ast->check_actual_formal_param(pro->get_formal_param_list());
				(yyval.ast) = ast;
				//ast->set_data_type(pro->get_return_type());
  		}
#line 1915 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 467 "parser.y" /* yacc.c:1646  */
    {

				list<Ast *>& alist = *(yyvsp[-1].ast_list);
				
				Sequence_Ast* ast = new Sequence_Ast(yylineno);
				for (list<Ast* >::iterator it=alist.begin(); it != alist.end(); ++it){
					ast->ast_push_back(*it);
				}
				(yyval.ast)= ast;
			

			}
#line 1932 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 480 "parser.y" /* yacc.c:1646  */
    {
				Sequence_Ast* ast = new Sequence_Ast(yylineno);
				ast->ast_push_back((yyvsp[0].ast));
				(yyval.ast) = ast;
			}
#line 1942 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 487 "parser.y" /* yacc.c:1646  */
    {(yyval.ast) = NULL;}
#line 1948 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 489 "parser.y" /* yacc.c:1646  */
    {
			(yyval.ast) = (yyvsp[0].ast);
			}
#line 1956 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 494 "parser.y" /* yacc.c:1646  */
    {(yyval.ast) = (yyvsp[-1].ast);}
#line 1962 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 496 "parser.y" /* yacc.c:1646  */
    {
				Logical_Expr_Ast* ast =  new 
				Logical_Expr_Ast((yyvsp[0].ast), _logical_not, NULL, yylineno);
				ast->set_data_type((yyvsp[0].ast)->get_data_type());
				(yyval.ast) = ast;
			}
#line 1973 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 503 "parser.y" /* yacc.c:1646  */
    {
				Logical_Expr_Ast* ast =  new 
				Logical_Expr_Ast((yyvsp[-2].ast), _logical_and, (yyvsp[0].ast), yylineno);
				if((yyvsp[-2].ast)->get_data_type() == (yyvsp[0].ast)->get_data_type())
	  				ast->set_data_type((yyvsp[-2].ast)->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
				(yyval.ast) = ast;
			}
#line 1986 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 512 "parser.y" /* yacc.c:1646  */
    {
				Logical_Expr_Ast* ast =  new 
				Logical_Expr_Ast((yyvsp[-2].ast), _logical_or, (yyvsp[0].ast), yylineno);
				if((yyvsp[-2].ast)->get_data_type() == (yyvsp[0].ast)->get_data_type())
	  				ast->set_data_type((yyvsp[-2].ast)->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
				(yyval.ast) = ast;
			}
#line 1999 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 58:
#line 524 "parser.y" /* yacc.c:1646  */
    {
				Relational_Expr_Ast* ast = new 
				Relational_Expr_Ast((yyvsp[-2].ast), less_than,(yyvsp[0].ast), yylineno);
				if((yyvsp[-2].ast)->get_data_type() == (yyvsp[0].ast)->get_data_type())
	  				ast->set_data_type((yyvsp[-2].ast)->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			(yyval.ast) = ast;

			}
#line 2013 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 534 "parser.y" /* yacc.c:1646  */
    {
				Relational_Expr_Ast* ast = new 
				Relational_Expr_Ast((yyvsp[-2].ast), greater_than,(yyvsp[0].ast), yylineno);
				if((yyvsp[-2].ast)->get_data_type() == (yyvsp[0].ast)->get_data_type())
	  				ast->set_data_type((yyvsp[-2].ast)->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			(yyval.ast) = ast;
			}
#line 2026 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 543 "parser.y" /* yacc.c:1646  */
    {
				Relational_Expr_Ast* ast = new 
				Relational_Expr_Ast((yyvsp[-2].ast), less_equalto,(yyvsp[0].ast), yylineno);
				if((yyvsp[-2].ast)->get_data_type() == (yyvsp[0].ast)->get_data_type())
	  				ast->set_data_type((yyvsp[-2].ast)->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			(yyval.ast) = ast;
			}
#line 2039 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 552 "parser.y" /* yacc.c:1646  */
    {
				Relational_Expr_Ast* ast = new 
				Relational_Expr_Ast((yyvsp[-2].ast), greater_equalto,(yyvsp[0].ast), yylineno);
				if((yyvsp[-2].ast)->get_data_type() == (yyvsp[0].ast)->get_data_type())
	  				ast->set_data_type((yyvsp[-2].ast)->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			(yyval.ast) = ast;
			}
#line 2052 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 561 "parser.y" /* yacc.c:1646  */
    {
				Relational_Expr_Ast* ast = new 
				Relational_Expr_Ast((yyvsp[-2].ast), not_equalto,(yyvsp[0].ast), yylineno);
				if((yyvsp[-2].ast)->get_data_type() == (yyvsp[0].ast)->get_data_type())
	  				ast->set_data_type((yyvsp[-2].ast)->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			(yyval.ast) = ast;
			}
#line 2065 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 570 "parser.y" /* yacc.c:1646  */
    {
				Relational_Expr_Ast* ast = new 
				Relational_Expr_Ast((yyvsp[-2].ast), equalto,(yyvsp[0].ast), yylineno);
				if((yyvsp[-2].ast)->get_data_type() == (yyvsp[0].ast)->get_data_type()){	  				
				ast->set_data_type((yyvsp[-2].ast)->get_data_type());
				}
	  			else{printf("cs316: Error\n");exit(0);}
	  			(yyval.ast) = ast;
			}
#line 2079 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 66:
#line 584 "parser.y" /* yacc.c:1646  */
    {(yyval.ast) = (yyvsp[-1].ast);}
#line 2085 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 585 "parser.y" /* yacc.c:1646  */
    {
				UMinus_Ast* ast = new UMinus_Ast((yyvsp[0].ast),NULL,yylineno);
	  			ast->set_data_type((yyvsp[0].ast)->get_data_type());
	  			(yyval.ast) = ast;
			}
#line 2095 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 591 "parser.y" /* yacc.c:1646  */
    {
				Conditional_Expression_Ast* ast = new 
				Conditional_Expression_Ast((yyvsp[-4].ast), (yyvsp[-2].ast), (yyvsp[0].ast), yylineno);
				if((yyvsp[-2].ast)->get_data_type() == (yyvsp[0].ast)->get_data_type())
	  				ast->set_data_type((yyvsp[-2].ast)->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			(yyval.ast) = ast;

			}
#line 2109 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 600 "parser.y" /* yacc.c:1646  */
    {
	  			Plus_Ast* ast = new Plus_Ast((yyvsp[-2].ast),(yyvsp[0].ast),yylineno);
	  			if((yyvsp[-2].ast)->get_data_type() == (yyvsp[0].ast)->get_data_type())
	  				ast->set_data_type((yyvsp[-2].ast)->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			(yyval.ast) = ast;
	  		}
#line 2121 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 70:
#line 607 "parser.y" /* yacc.c:1646  */
    {
	  			Minus_Ast* ast = new Minus_Ast((yyvsp[-2].ast),(yyvsp[0].ast),yylineno);
	  			if((yyvsp[-2].ast)->get_data_type() == (yyvsp[0].ast)->get_data_type())
	  				ast->set_data_type((yyvsp[-2].ast)->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			(yyval.ast) = ast;
	  		}
#line 2133 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 614 "parser.y" /* yacc.c:1646  */
    {
	  			Divide_Ast* ast = new Divide_Ast((yyvsp[-2].ast),(yyvsp[0].ast),yylineno);
	  			if((yyvsp[-2].ast)->get_data_type() == (yyvsp[0].ast)->get_data_type())
	  				ast->set_data_type((yyvsp[-2].ast)->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			(yyval.ast) = ast;
	  		}
#line 2145 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 621 "parser.y" /* yacc.c:1646  */
    {
	  			Mult_Ast* ast = new Mult_Ast((yyvsp[-2].ast),(yyvsp[0].ast),yylineno);
	  			if((yyvsp[-2].ast)->get_data_type() == (yyvsp[0].ast)->get_data_type())
	  				ast->set_data_type((yyvsp[-2].ast)->get_data_type());
	  			else{printf("cs316: Error\n");exit(0);}
	  			(yyval.ast) = ast;
	  		}
#line 2157 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 629 "parser.y" /* yacc.c:1646  */
    {
	  				if(!program_object.is_procedure_exists(*(yyvsp[-3].string_value))){
	  					printf("cs316: Error\n");
	  					exit(0);
	  				}
	  				Procedure* pro = program_object.get_procedure_prototype(*(yyvsp[-3].string_value));
	  				pro->set_proc_is_called();
	  				Call_Ast* ast = new Call_Ast(*(yyvsp[-3].string_value),yylineno);
					ast->set_actual_param_list(*(yyvsp[-1].ast_list));
					ast->check_actual_formal_param(pro->get_formal_param_list());
					(yyval.ast) = ast;
					ast->set_data_type(pro->get_return_type());
	  			}
#line 2175 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 645 "parser.y" /* yacc.c:1646  */
    {
				list<Ast *>* alist = new list<Ast*>();
				(yyval.ast_list) = alist;
			}
#line 2184 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 649 "parser.y" /* yacc.c:1646  */
    {
				list<Ast *>* alist = new list<Ast*>();
				alist->push_back((yyvsp[0].ast));
				(yyval.ast_list) = alist;
			}
#line 2194 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 76:
#line 654 "parser.y" /* yacc.c:1646  */
    {
				list<Ast *>& alist = *(yyvsp[-2].ast_list);
				alist.push_back((yyvsp[0].ast));
			}
#line 2203 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 77:
#line 660 "parser.y" /* yacc.c:1646  */
    {
				Name_Ast* ast;

				if(ltable && ltable->variable_in_symbol_list_check(* (yyvsp[0].string_value))){
					ast = new Name_Ast(*(yyvsp[0].string_value),ltable->get_symbol_table_entry(* (yyvsp[0].string_value)),yylineno);
				}
				else if(ptable && ptable->variable_in_symbol_list_check(*(yyvsp[0].string_value))){
					ast = new Name_Ast(*(yyvsp[0].string_value),ptable->get_symbol_table_entry(* (yyvsp[0].string_value)),yylineno);
				}
				else if(gtable && gtable->variable_in_symbol_list_check(*(yyvsp[0].string_value))){
					ast = new Name_Ast(*(yyvsp[0].string_value),gtable->get_symbol_table_entry(* (yyvsp[0].string_value)),yylineno);
				}
				else{
					printf("cs316: Error\n");
					exit(0);
				}
				(yyval.ast) = ast;
			    }
#line 2226 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 78:
#line 680 "parser.y" /* yacc.c:1646  */
    {
					Number_Ast<int> *ast = new Number_Ast<int>((yyvsp[0].integer_value),int_data_type,yylineno); 
					(yyval.ast) = ast;
				}
#line 2235 "parser.tab.c" /* yacc.c:1646  */
    break;

  case 79:
#line 685 "parser.y" /* yacc.c:1646  */
    {
					Number_Ast<double> *ast = new Number_Ast<double>((yyvsp[0].double_value),double_data_type,yylineno); 
					(yyval.ast) = ast;
				}
#line 2244 "parser.tab.c" /* yacc.c:1646  */
    break;


#line 2248 "parser.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 691 "parser.y" /* yacc.c:1906  */


